@echo off
set /p JDK_PATH=JDK eleresi utja? (Hagyd uresen, ha a PATH tartalmazza) 
if NOT "%JDK_PATH%" == "" set JAVAC_PATH=%JDK_PATH%\bin\javac.exe
if "%JDK_PATH%" == "" set JAVAC_PATH=javac.exe
if NOT "%JDK_PATH%" == "" set JAR_PATH=%JDK_PATH%\bin\jar.exe
if "%JDK_PATH%" == "" set JAR_PATH=jar.exe
@rmdir /S /Q bin
@md bin
cd bin
"%JAVAC_PATH%" ../src/shared/*.java ../src/model/*.java ../src/view/*.java ../src/controller/*.java -d ./
if %errorlevel% EQU 0 goto BJ
:ERR
echo Hiba!
goto END
:BJ
	"%JAR_PATH%" cfe ../pear.jar controller.Controller controller/*.class shared/*.class model/*.class view/*.class
:SUC
	echo Sikerult.
:END
	cd ..