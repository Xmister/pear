package shared;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;



/*
 * A konzolos, illetve f�jlm�veletek megk�nny�t�s�re l�trehozott, tiszt�n statikus oszt�ly.
 * abstract, hogy m�g v�letlen�l se lehessen megval�s�tani.
 */
public abstract class PearConsoleHelper {
	
	public static PearEventAdapter pe = new PearEventAdapter() {
		public void beforeExit(PearEvent e) {
			closeFile();
		}
	};
	
	public static boolean disabled=false;
	private static PrintStream out=System.out;
	private static boolean beszedes=false;
	
	public static String textQuestion(String question) {
		out.println(question + "\n");
		try {
			return new BufferedReader(new InputStreamReader(System.in)).readLine();
		} catch (IOException e) {
			return e.getStackTrace().toString();
		}
	}
	
	public static int numQuestion(String question, int a, int b) {
		int res=-1;
		while (res == -1) {
			out.println(question + "\n");
			try {
				res=Integer.valueOf(new BufferedReader(new InputStreamReader(System.in)).readLine());
				if (res < a || res > b) throw new NumberFormatException();
			} 	catch (NumberFormatException e) {
				out.println("Kerlek, csak " + a + " es " + b + " kozti egesz szamot adj meg!\n");
				res=-1;
			}	catch (IOException e) {
				e.printStackTrace();
			}
		}
		return res;
	}
	
	public static void setBeszedes(boolean beszedes) {
		PearConsoleHelper.beszedes = beszedes;
	}

	public static void I(String msg) {
		out.println("[INF] "+msg);
	}
	
	public static void B(String msg) {
		if ( beszedes ) I(msg);
	}
	
	public static void E(String msg) {
		out.println("[ERR] "+msg);
	}
	
	public static boolean setFileOut(String file) {
		try {
			out=new PrintStream(file);
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static void closeFile() {
		if (out != System.out) {
			out.close();
			out=System.out;
		}
	}
}
