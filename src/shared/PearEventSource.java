package shared;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import model.Boly;
import model.Elem;
import model.Eszkoz;
import model.Fold;
import model.Hangya;
import model.Hangyaszsun;
import model.Palya;
import model.Property;
import model.Raktar;


public class PearEventSource {
	private static List<PearEventListener> _listeners = new ArrayList<PearEventListener>();
	private static Object fakeSource = new Object();

	public static synchronized void addEventListener(PearEventListener listener) {
		_listeners.add(listener);
	}

	public static synchronized void addEventListeners(List<PearEventListener> listeners) {
		_listeners.addAll(listeners);
	}

	public static synchronized List<PearEventListener> getEventListeners() {
		return new ArrayList<PearEventListener>(_listeners);
	}

	public static synchronized void removeEventListener(PearEventListener listener) {
		_listeners.remove(listener);
	}

	public static synchronized void fireCellChanged(int x, int y) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onCellChanged(event, x, y);
		}
	}

	public static synchronized void fireMapCreated(Palya p) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onMapCreated(event, p);
		}
	}

	public static synchronized void fireToolsCreated(List<Eszkoz> eszkozok) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onToolsCreated(event, eszkozok);
		}
	}

	public static synchronized void fireElemKilled(Elem e) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onElemKilled(event, e);
		}
	}

	public static synchronized void fireElemAdded(Elem e) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onElemAdded(event, e);
		}
	}

	public static synchronized void fireElemMoved(Elem e, int newX, int newY) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onElemMoved(event, e, newX, newY);
		}
	}

	public static synchronized void fireMapStep(int step) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onMapStep(event, step);
		}
	}

	public static synchronized void fireExit() {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().beforeExit(event);
		}
		_listeners.clear(); // Ne kapjanak tobb eventet
	}

	public static synchronized void fireReadCommandsBegin(String fileName) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onReadCommandsBegin(event,fileName);
		}

	}
	
	public static synchronized void fireReadCommandsFinished(String fileName, boolean success) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onReadCommandsFinished(event,fileName,success);
		}

	}
	
	public static synchronized void fireGenerateFinished(boolean success) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onGenerateFinished(event,success);
		}

	}

	public static synchronized void fireWriteCommandsFinished(String fileName) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onWriteCommands(event,fileName);
		}

	}

	public static synchronized void fireEndWriteCommandsFinished() {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onEndWriteCommands(event);
		}

	}

	public static synchronized void fireAddAntFinished(Hangya h) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onAddAntFinished(event,h);
		}

	}

	public static synchronized void fireAddAntHillFinished(Boly b) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onAddAntHillFinished(event,b);
		}

	}

	public static synchronized void fireAddFoodStorageFinished(Raktar r) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onAddFoodStorageFinished(event,r);
		}

	}

	public static synchronized void fireAddAntEaterFinished(Hangyaszsun s) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onAddAntEaterFinished(event,s);
		}

	}

	public static synchronized void fireAddAntLionFinished(Fold f) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onAddAntLionFinished(event,f);
		}

	}

	public static synchronized void fireAddGroundFinished(Fold f) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onAddGroundFinished(event,f);
		}

	}

	public static synchronized void fireAddStoneFinished(Fold f) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onAddStoneFinished(event,f);
		}

	}

	public static synchronized void fireAddWaterFinished(Fold f) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onAddWaterFinished(event,f);
		}

	}

	public static synchronized void fireUsePoisonFinished(int x, int y, int count) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onUsePoisonFinished(event,x,y,count);
		}

	}

	public static synchronized void fireUseSmellFinished(int x, int y, int count) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onUseSmellFinished(event,x,y,count);
		}

	}
	
	public static synchronized void fireShowMapBegin() {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onShowMapBegin(event);
		}

	}

	public static synchronized void fireShowMapFinished() {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onShowMapFinished(event);
		}

	}

	public static synchronized void fireAddPropertyFinished(Elem e, Property p) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onAddPropertyFinished(event,e,p);
		}

	}

	public static synchronized void fireSetRandomFinished(boolean random) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onSetRandomFinished(event,random);
		}

	}

	public static synchronized void fireSetDirFinished() {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onSetDirFinished(event);
		}

	}

	public static synchronized void fireFoodDelivered(Boly boly) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onFoodDelivered(event,boly);
		}
	}
	
	public static synchronized void fireError(ErrorLevel level, String message) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onError(event,level,message);
		}
	}

	public static synchronized void fireShowCell(String out) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onShowCell(event,out);
		}
	}

	public static synchronized void fireStorageChanged(Raktar raktar) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onStorageChanged(event,raktar);
		}
	}
	
	public static synchronized void fireGameOver(int steps) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onGameOver(event,steps);
		}
	}

	public static synchronized void fireSunAte(Hangyaszsun hangyaszsun) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onSunAte(event,hangyaszsun);
		}
	}

	public static synchronized void fireClicked(Elem elem) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onClicked(event,elem);
		}
	}

	public static synchronized void fireFoodCountChanged(int foodCount) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onFoodCountChanged(event,foodCount);
		}
	}

	public static synchronized void fireSpeedChange(long timer_step) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onSpeedChange(event,timer_step);
		}		
	}

	public static synchronized void firePauseChanged(boolean pause) {
		PearEvent event = new PearEvent(fakeSource);
		Iterator<PearEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			i.next().onPauseChange(event,pause);
		}
	}
}
