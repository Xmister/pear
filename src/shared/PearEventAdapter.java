package shared;


import java.util.List;

import model.Boly;
import model.Elem;
import model.Eszkoz;
import model.Fold;
import model.Hangya;
import model.Hangyaszsun;
import model.Palya;
import model.Property;
import model.Raktar;


public  class PearEventAdapter implements PearEventListener {

	@Override
	public void onCellChanged(PearEvent e, int x, int y) {
	}

	@Override
	public void onMapCreated(PearEvent e, Palya p) {
	}

	@Override
	public void onToolsCreated(PearEvent e, List<Eszkoz> eszkozok) {
		
	}

	@Override
	public void beforeExit(PearEvent e) {
		
	}

	@Override
	public void onElemKilled(PearEvent ev, Elem e) {
		
	}
	
	@Override
	public void onElemAdded(PearEvent ev, Elem e) {
		
	}
	
	@Override
	public void onElemMoved(PearEvent ev, Elem e, int newX, int newY) {
		
	}

	@Override
	public void onMapStep(PearEvent ev, int step) {
		
	}

	@Override
	public void onReadCommandsFinished(PearEvent event, String fileName, boolean success) {
		
	}
	
	@Override
	public void onGenerateFinished(PearEvent event, boolean success) {
		
	}

	@Override
	public void onWriteCommands(PearEvent event, String fileName) {
		
	}

	@Override
	public void onEndWriteCommands(PearEvent event) {
		
	}

	@Override
	public void onAddAntFinished(PearEvent event, Hangya h) {
		
	}

	@Override
	public void onAddAntHillFinished(PearEvent event, Boly b) {
		
	}

	@Override
	public void onAddFoodStorageFinished(PearEvent event, Raktar r) {
		
	}

	@Override
	public void onAddAntEaterFinished(PearEvent event, Hangyaszsun s) {
		
	}

	@Override
	public void onAddAntLionFinished(PearEvent event, Fold f) {
		
	}

	@Override
	public void onAddGroundFinished(PearEvent event, Fold f) {
		
	}

	@Override
	public void onAddStoneFinished(PearEvent event, Fold f) {
		
	}

	@Override
	public void onAddWaterFinished(PearEvent event, Fold f) {
		
	}

	@Override
	public void onUsePoisonFinished(PearEvent event, int x, int y, int count) {
		
	}

	@Override
	public void onUseSmellFinished(PearEvent event, int x, int y, int count) {
		
	}

	@Override
	public void onShowMapFinished(PearEvent event) {
		
	}

	@Override
	public void onAddPropertyFinished(PearEvent event, Elem e, Property p) {
		
	}

	@Override
	public void onSetRandomFinished(PearEvent event, boolean random) {
		
	}

	@Override
	public void onSetDirFinished(PearEvent event) {
		
	}

	@Override
	public void onFoodDelivered(PearEvent event, Boly boly) {
		
	}

	@Override
	public void onError(PearEvent event, ErrorLevel level, String message) {
		
	}

	@Override
	public void onShowCell(PearEvent event, String out) {
		
	}

	@Override
	public void onStorageChanged(PearEvent event, Raktar raktar) {
		
	}

	@Override
	public void onShowMapBegin(PearEvent event) {
		
	}

	@Override
	public void onReadCommandsBegin(PearEvent event, String fileName) {

	}

	@Override
	public void onGameOver(PearEvent event, int steps) {
		
	}

	@Override
	public void onSunAte(PearEvent event, Hangyaszsun hangyaszsun) {

	}

	@Override
	public void onClicked(PearEvent event, Elem elem) {
		
	}

	@Override
	public void onFoodCountChanged(PearEvent event, int foodCount) {

	}

	@Override
	public void onSpeedChange(PearEvent event, long timer_step) {

	}

	@Override
	public void onPauseChange(PearEvent event, boolean pause) {

	}

}
