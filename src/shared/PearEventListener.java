package shared;


import java.util.List;

import model.Boly;
import model.Elem;
import model.Eszkoz;
import model.Fold;
import model.Hangya;
import model.Hangyaszsun;
import model.Palya;
import model.Property;
import model.Raktar;



public interface PearEventListener {
	public void onCellChanged(PearEvent e, int x, int y);
	public void onMapCreated(PearEvent e, Palya p);
	public void onToolsCreated(PearEvent e, List<Eszkoz> eszkozok);
	public void beforeExit(PearEvent e);
	public void onElemKilled(PearEvent ev, Elem e);
	public void onElemAdded(PearEvent ev, Elem e);
	public void onElemMoved(PearEvent ev, Elem e, int newX, int newY);
	public void onMapStep(PearEvent ev, int step);
	public void onReadCommandsBegin(PearEvent event, String fileName);
	public void onReadCommandsFinished(PearEvent event, String fileName, boolean success);
	public void onWriteCommands(PearEvent event, String fileName);
	public void onEndWriteCommands(PearEvent event);
	public void onAddAntFinished(PearEvent event, Hangya h);
	public void onAddAntHillFinished(PearEvent event, Boly b);
	public void onAddFoodStorageFinished(PearEvent event, Raktar r);
	public void onAddAntEaterFinished(PearEvent event, Hangyaszsun s);
	public void onAddAntLionFinished(PearEvent event, Fold f);
	public void onAddGroundFinished(PearEvent event, Fold f);
	public void onAddStoneFinished(PearEvent event, Fold f);
	public void onAddWaterFinished(PearEvent event, Fold f);
	public void onUsePoisonFinished(PearEvent event, int x, int y, int count);
	public void onUseSmellFinished(PearEvent event, int x, int y, int count);
	public void onShowMapBegin(PearEvent event);
	public void onShowMapFinished(PearEvent event);
	public void onAddPropertyFinished(PearEvent event, Elem e, Property p);
	public void onSetRandomFinished(PearEvent event, boolean random);
	public void onSetDirFinished(PearEvent event);
	public void onFoodDelivered(PearEvent event, Boly boly);
	public void onError(PearEvent event, ErrorLevel level, String message);
	public void onShowCell(PearEvent event, String out);
	public void onStorageChanged(PearEvent event, Raktar raktar);
	public void onGenerateFinished(PearEvent event, boolean success);
	public void onGameOver(PearEvent event, int steps);
	public void onSunAte(PearEvent event, Hangyaszsun hangyaszsun);
	public void onClicked(PearEvent event, Elem elem);
	public void onFoodCountChanged(PearEvent event, int foodCount);
	public void onSpeedChange(PearEvent event, long timer_step);
	public void onPauseChange(PearEvent event, boolean pause);
}
