package shared;

import java.io.PrintWriter;
import java.io.StringWriter;

public class PearException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1423472986251581610L;
	
	public PearException(String message) {
		super(message);
	}
	
	public static String stackTraceToString(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		return sw.toString();
	}
}
