package controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;

import shared.ErrorLevel;
import shared.PearEventSource;
import shared.PearException;
import shared.PearConsoleHelper;

import model.Boly;
import model.Eszkoz;
import model.Fold;
import model.Hangya;
import model.Hangyaszsun;
import model.MyBoolean;
import model.Palya;
import model.Property;
import model.Raktar;
import model.Type;
import model.Dir;

public class Command {
	/*
	 * newTrack <width> <height> <new>
		Le�r�s: �j p�ly�t hoz l�tre a megadott m�rettel.
		width: A p�lya sz�less�ge
		height: A p�lya magass�ga
		new: Ha true, akkor teljesen �j p�lya (felt�lti Fold objektumokkal). Ha false, akkor k�s�bb felt�lthet� egyedi objektumokkal.
	*/
	public static boolean newtrack(String[] args) throws PearException {
		String err="A Palya letrehozasa meghiusult";
		try {
			if (Controller.getPalya() != null ) Controller.getPalya().finalize();
			final Palya palya = new Palya(Integer.valueOf(args[0]),Integer.valueOf(args[1]),MyBoolean.valueOf(args[2].toLowerCase()));
			PearEventSource.fireMapCreated(palya);
			return true;
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * generate <raktar> <tocsa> <ko>
		Le�r�s: �j p�ly�t gener�l a megadott param�terekkel.
		raktar: Rakt�rak sz�moss�ga
		tocsa: T�cs�k sz�moss�ga
		ko: K�vek sz�moss�ga
	*/
	public static boolean generate(String[] args) throws PearException {
		String err="A palya generalas meghiusult";
		boolean ret=true;
		try {
			final Palya palya = Controller.getPalya();
			palya.generate(Integer.valueOf(args[0]), Integer.valueOf(args[1]), Integer.valueOf(args[2]), Integer.valueOf(args[3]));
		} catch (Exception e) {
			ret=false;
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
		finally {
			PearEventSource.fireGenerateFinished(ret);
		}
		return ret;
	}
	
	/*
	 * readCommands  <fileName>
	 *	Le�r�s: V�grehajtja a megadott f�jlban l�v� parancsokat
	 *	fileName: az el�re elk�sz�tett teszteset f�jlja
	 */
	public static boolean readcommands(String[] args) throws PearException {
			String err="A "+args[0]+" nevu fajl betoltese nem sikerult.";
			String fcmd=null;
			boolean ret=true;
			PearEventSource.fireReadCommandsBegin(args[0]);
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(args[0])));
				String line;
				while ( ret && (line=in.readLine()) != null ) {
						String[] tokens=line.split(" ");
						fcmd=tokens[0];
						String[] fargs = Arrays.copyOfRange(tokens, 1, tokens.length);
						if (fcmd.length() > 0)
							ret=(Boolean)Command.class.getMethod(fcmd.toLowerCase(),String[].class).invoke(null, (Object)fargs);
				}
			} catch (NoSuchMethodException ne) {
				ret=false;
				Controller.errorMessage(ErrorLevel.E_HIGH, "Mi ez? "+fcmd+" parancs nincs.");
			}
			catch (Exception e) {
				ret=false;
				throw new PearException(err+"\n"+PearException.stackTraceToString(e));
			}
			finally {
				PearEventSource.fireReadCommandsFinished(args[0], ret);
			}
			return ret;
	}
	
	/*
	 * writeCommands <fileName>
		Le�r�s: A teszt eredm�ny�t f�jlba �rja
		fileName: Ebbe a f�jlba �rja az eredm�nyt
	 */
	public static boolean writecommands(String[] args) throws PearException {
		String err="Faljba iras sikertelen.";
		try {
			if (PearConsoleHelper.setFileOut(args[0])) {
				PearEventSource.fireWriteCommandsFinished(args[0]);
				return true;
			}
			else throw new PearException(args[0]+" fajl nem hozhato letre.");
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * endWriteCommands
		Le�r�s: A f�jlba �r�s befejez�se
	 */
	public static boolean endwritecommands(String[] args) throws PearException {
		String err="Falj lezaras sikertelen.";
		try {
			PearConsoleHelper.closeFile();
			PearEventSource.fireEndWriteCommandsFinished();
			return true;
		} catch ( Exception e ) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * addAnt <x> <y> <carry> [properties]
		Le�r�s: Hangya hozz�ad�sa a p�ly�hoz
		x: A hangya x koordin�t�ja a p�ly�n
		y: A hangya y koordin�t�ja a p�ly�n
		carry: Jelzi, hogy az adott hangya �ppen cipel-e �lelmet.
		properties: Opcion�lis, a megadott property-ket az elemhez adja.
	 */
	public static boolean addant(String[] args) throws PearException {
		String err="Hangya hozzaadasa sikertelen.";
		try {
			Hangya h = new Hangya(Integer.valueOf(args[0]),Integer.valueOf(args[1]));
			h.setCarry(MyBoolean.valueOf(args[2]));
			if ( Controller.getPalya().getBoly() != null ) {
				h.setStartPos(Controller.getPalya().getBoly().getX(), Controller.getPalya().getBoly().getY());
			}
			for (int i=3; i<args.length; ++i) {
				try {
					h.addProperty(Property.valueOf(args[i]));
				} catch ( IllegalArgumentException e) {				 	//Nincs ilyen property
					h.setId(Integer.valueOf(args[i]));					//Akkor csak ID lehet
				}
			}
			if (Controller.getPalya().addElem(h)) {
				PearEventSource.fireAddAntFinished(h);
				return true;
			}
			else throw new PearException("Hangya hozzaadasa nem sikerult.\n"+h.getX()+", "+h.getY()+" pozicio palyan kivul van, vagy mar van valami.");
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * addAnthill <x> <y> <intensity> <deliveredAmount:>
		Le�r�s: Hangyaboly hozz�ad�sa a p�ly�hoz
		x: A boly x koordin�t�ja a p�ly�n
		y: A boly y koordin�t�ja a p�ly�n
		intensity: A hangy�k kibocs�t�si intezit�s�t hat�rozza meg.
		deliveredAmount: Az egyes hangy�k �ltal lesz�ll�tott �lelem mennyis�ge.
	 */
	public static boolean addanthill(String[] args) throws PearException {
		String err="Boly hozzaadasa sikertelen.";
		try {
			Boly b = new Boly(Integer.valueOf(args[0]),Integer.valueOf(args[1]));
			b.setItenseity(Integer.valueOf(args[2]));
			b.setDeliveredAmount(Integer.valueOf(args[3]));
			for (int i=4; i<args.length; ++i) {
				try {
					b.addProperty(Property.valueOf(args[i]));
				} catch ( IllegalArgumentException e) { 				//Nincs ilyen property
					b.setId(Integer.valueOf(args[i]));					//Akkor csak ID lehet
				}
			}
			if (Controller.getPalya().addElem(b)) {
				PearEventSource.fireAddAntHillFinished(b);
				return true;
			}
			else throw new PearException("Hangyaboly hozzaadasa nem sikerult.\n"+b.getX()+", "+b.getY()+" pozicio palyan kivul van, vagy mar van valami.");
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * addFoodStorage <x> <y> <capacity> <leftAmount>
		Le�r�s: �telrakt�r hozz�ad�sa a p�ly�hoz
		x: A rakt�r x koordin�t�ja a p�ly�n
		y: A rakt�r y koordin�t�ja a p�ly�n
		capacity: A rakt�rban l�v� maxim�lis �lelmiszer mennyis�ge.
		leftAmount: A rakt�rban m�g megmaradt �lelmiszer mennyis�ge.
	 */
	public static boolean addfoodstorage(String[] args) throws PearException {
		String err="Raktar hozzaadasa sikertelen.";
		try {
			Raktar r = new Raktar(Integer.valueOf(args[0]),Integer.valueOf(args[1]));
			r.setCapacity(Integer.valueOf(args[2]));
			r.setLeftAmount(Integer.valueOf(args[3]));
			for (int i=4; i<args.length; ++i) {
				try {
					r.addProperty(Property.valueOf(args[i]));
				} catch ( IllegalArgumentException e) { 				//Nincs ilyen property
					r.setId(Integer.valueOf(args[i]));					//Akkor csak ID lehet
				}
			}
			if (Integer.valueOf(args[2])<=0) throw new PearException("Raktar hozzaad�sa nem sikerult. A kapacitas tul kicsi.");
			else if (Integer.valueOf(args[3])>Integer.valueOf(args[2])) throw new PearException("Raktar hozzaad�sa nem sikerult. A megmaradt mennyiseg nagyobb, mint a kapacitas.");
			else if (Controller.getPalya().addElem(r)) {
				PearEventSource.fireAddFoodStorageFinished(r);
				return true;
			}
			else throw new PearException("Raktar hozzaadasa nem sikerult.\n"+r.getX()+", "+r.getY()+" pozicio palyan kivul van, vagy mar van valami.");
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * addAntEater <x> <y> <bellySize> <antCount> [properties]
		Le�r�s: Hangy�szs�n hozz�ad�sa a p�ly�hoz
		x: A s�n x koordin�t�ja a p�ly�n
		y: A s�n y koordin�t�ja a p�ly�n
		bellySize: A s�n bend�m�rete
		antCount: Az elfogyasztott hangy�k sz�ma.
		properties: Opcion�lis, a megadott property-ket az elemhez adja.
	 */
	public static boolean addanteater(String[] args) throws PearException {
		String err="Hangyaszsun hozzaadasa sikertelen.";
		try {
			Hangyaszsun s = new Hangyaszsun(Integer.valueOf(args[0]),Integer.valueOf(args[1]));
			s.setBellySize(Integer.valueOf(args[2]));
			s.setAntCount(Integer.valueOf(args[3]));
			for (int i=4; i<args.length; ++i) {
				try {
					s.addProperty(Property.valueOf(args[i]));
				} catch ( IllegalArgumentException e) { //Nincs ilyen property
					s.setId(Integer.valueOf(args[i]));	//Akkor csak ID lehet
				}
			}
			if (s.getBellySize()<0) throw new PearException("HangyaszSun hozzaadasa nem sikerult. A bendomeret tul kicsi.");
			else if (s.getAntCount()>s.getBellySize()) throw new PearException("HangyaszSun hozzaadasa nem sikerult. A megevett hangyak menynisege nagyobb, mint a bendo nagysaga.");
			else if (Controller.getPalya().validPos(s, s.getX(), s.getY()) == null) throw new PearException("HangyaszSun hozzaadasa nem sikerult.\n"+s.getX()+", "+s.getY()+" pozicio palyan kivul van.");
			else if (Controller.getPalya().addElem(s)) {
				PearEventSource.fireAddAntEaterFinished(s);
				return true;
			}
			else throw new PearException("HangyaszSun hozzaadase nem sikerult. Mar van hangyaszsun a palyan, vagy a megadott pozicio nem ures.");
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * addAntLion <x> <y> [properties]
		Le�r�s: Hangyales� hozz�ad�sa a p�ly�hoz
		x: A hangyales� x koordin�t�ja a p�ly�n
		y: A hangyales� y koordin�t�ja a p�ly�n
	 */
	public static boolean addantlion(String[] args) throws PearException {
		String err="Hangyaleso hozzaadasa sikertelen.";
		try {
			Fold f = new Fold(Integer.valueOf(args[0]),Integer.valueOf(args[1]),Type.HANGYALESO);
			for (int i=2; i<args.length; ++i) {
				try {
					f.addProperty(Property.valueOf(args[i]));
				} catch ( IllegalArgumentException e) { 						//Nincs ilyen property
					if ( i == 2 && args.length > 3 ) f.setSzagOwner(Integer.valueOf(args[i]));
					else f.setId(Integer.valueOf(args[i]));						//Ha van m�g, az a saj�tunk.
				}
			}
			if (Controller.getPalya().addElem(f)) {
				PearEventSource.fireAddAntLionFinished(f);
				return true;
			}
			else {
				throw new PearException("Hangyaleso hozzaadasa nem sikerult.\n"+f.getX()+", "+f.getY()+" koordinatak a  palyan kivul vannak, vagy mar letezik valami az adott koordinatan.");
			}
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * addGround <x> <y> <step> [properties]
		Le�r�s: F�ld hozz�ad�sa a p�ly�hoz
		x: A f�ld x koordin�t�ja a p�ly�n
		y: A f�ld y koordin�t�ja a p�ly�n
		step: Jelzi mennyit �l�pt�nk� az id�ben
		properties: Opcion�lis, a megadott property-ket az elemhez adja.
	 */
	public static boolean addground(String[] args) throws PearException {
		String err="Fold hozzaadasa sikertelen.";
		try {
			Fold f = new Fold(Integer.valueOf(args[0]),Integer.valueOf(args[1]));
			f.setSteps(Integer.valueOf(args[2]));
			for (int i=3; i<args.length; ++i) {
				try {
					f.addProperty(Property.valueOf(args[i]));
				} catch ( IllegalArgumentException e) { 						//Nincs ilyen property
					if ( i == 3 && args.length > 4 ) f.setSzagOwner(Integer.valueOf(args[i]));	//Az els� ID csak a szagOwner lehet
					else f.setId(Integer.valueOf(args[i]));						//Ha van m�g, az a saj�tunk.
				}
			}
			if (Controller.getPalya().addElem(f)) {
				PearEventSource.fireAddGroundFinished(f);
				return true;
			}
			else {
				throw new PearException("Fold hozzaadasa nem sikerult.\n"+f.getX()+", "+f.getY()+" koordinatak a palyan kivul vannak, vagy mar letezik fold az adott koordinatan.");
			}
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * addStone <x> <y> [properties]
		Le�r�s: Kavics hozz�ad�sa a p�ly�hoz
		x: A kavics x koordin�t�ja a p�ly�n
		y: A kavics y koordin�t�ja a p�ly�n
		properties: Opcion�lis, a megadott property-ket az elemhez adja.
	 */
	public static boolean addstone(String[] args) throws PearException {
		String err="Kavics hozzaadasa sikertelen.";
		try {
			Fold f = new Fold(Integer.valueOf(args[0]),Integer.valueOf(args[1]),Type.KAVICS);
			for (int i=2; i<args.length; ++i) {
				try {
					f.addProperty(Property.valueOf(args[i]));
				} catch ( IllegalArgumentException e) { 						//Nincs ilyen property
					if ( i == 2 && args.length > 3  ) f.setSzagOwner(Integer.valueOf(args[i]));	//Az els� ID csak a szagOwner lehet
					else f.setId(Integer.valueOf(args[i]));						//Ha van m�g, az a saj�tunk.
				}
			}
			if (Controller.getPalya().addElem(f)) {
				PearEventSource.fireAddStoneFinished(f);
				return true;
			}
			else {
				throw new PearException("Kavics hozzaadasa sikertelen.\n"+f.getX()+", "+f.getY()+" koordinatak a palyan kivul vannak, vagy mar letezik valamiaz adott koordinatan.");
			}
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * addWater <x> <y> [properties]
		Le�r�s: V�z hozz�ad�sa a p�ly�hoz
		x: A v�z x koordin�t�ja a p�ly�n
		y: A v�z y koordin�t�ja a p�ly�n
		properties: Opcion�lis, a megadott property-ket az elemhez adja.
	 */
	public static boolean addwater(String[] args) throws PearException {
		String err="Viz hozzaadasa sikertelen.";
		try{
			Fold f = new Fold(Integer.valueOf(args[0]),Integer.valueOf(args[1]),Type.TOCSA);
			for (int i=2; i<args.length; ++i) {
				try {
					f.addProperty(Property.valueOf(args[i]));
				} catch ( IllegalArgumentException e) { 						//Nincs ilyen property
					if ( i == 2 && args.length > 3  ) f.setSzagOwner(Integer.valueOf(args[i]));	//Az els� ID csak a szagOwner lehet
					else f.setId(Integer.valueOf(args[i]));						//Ha van m�g, az a saj�tunk.
				}
			}
			if (Controller.getPalya().addElem(f)) {
				PearEventSource.fireAddWaterFinished(f);
				return true;
			}
			else {
				throw new PearException("Viz hozzaadasa sikertelen.\n"+f.getX()+", "+f.getY()+" koordinatak a palyan kivul vannak, vagy mar letezik valami az adott koordinatan.");
			}
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * addTools <count_poison> <rad_poison> <count_smell> <rad_smell>
		Le�r�s: Spray-k l�trehoz�sa
		count_poison: A hagya�rt� spray marad�k kapacit�sa
		rad_poison: A hagya�rt� spray hat�k�r�nek sugara cell�k sz�m�ban
		count_smell: A hagyaszag-semleges�t� spray marad�k kapacit�sa
		rad_smell:: A hagyaszag-semleges�t� spray hat�k�r�nek sugara cell�k sz�m�ban
	 */
	public static boolean addtools(String[] args) throws PearException {
		String err="Eszkozok hozzaadasa sikertelen.";
		try {
			ArrayList<Eszkoz> eszkozok = new ArrayList<Eszkoz>(2);
			Eszkoz p = new Eszkoz(Controller.getPalya(), Integer.valueOf(args[0]), Integer.valueOf(args[1]));
			p.setProperty(Property.MERGEZETT);
			Eszkoz s = new Eszkoz(Controller.getPalya(), Integer.valueOf(args[2]), Integer.valueOf(args[3]));
			s.setProperty(Property.SZAGTALAN);
			if (p.getCount()<0 || s.getCount()<0) {
				throw new PearException("Eszkozok hozzaadasa sikertelen. A szamlalo erteke tul alacsony.");
			}
			else if (p.getRadius()<0 || s.getRadius()<0) {
				throw new PearException("Eszkozok hozzaadasa sikertelen. A hatokor tul kicsi.");
			}
			else if (eszkozok.add(p)) {
					if ( eszkozok.add(s)) {
					PearEventSource.fireToolsCreated(eszkozok);
					return true;
					}
					else
						throw new PearException(err);
			}
			else 
				throw new PearException(err);
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * usePoison <x> <y>
		Le�r�s: Megadott koordint�n a Hangyairto haszn�lata.
		x: A P�lya x koordin�t�ja
		y: A P�lya y koordin�t�ja
	 */
	public static boolean usepoison(String[] args) throws PearException {
		String err="Hangyairto spray hasznalata nem sikerult.";
		try {
			boolean van=false;
			
			if (Controller.getPalya().validPos(null,Integer.valueOf(args[0]), Integer.valueOf(args[1])) == null) throw new PearException("Hangyairto spray hasznalata nem sikerult. A koordinatak a palyan kivul vannak.");
			else {
				for (Eszkoz e: Controller.getTools()) {
					if (e.getProperty() == Property.MERGEZETT) {
						van=true;
						if (e.getCount()==0) throw new PearException("Hangyairto spray mar kiurult");
						e.use(Integer.valueOf(args[0]), Integer.valueOf(args[1]));
						PearEventSource.fireUsePoisonFinished(Integer.valueOf(args[0]), Integer.valueOf(args[1]), e.getCount());
						return true;
					}
				}
			}
			if (!van) throw new PearException("Hangyairto spray meg nincs letrehozva.");
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
		throw new PearException(err);
	}
	
	/*
	 * useSmell <x> <y>
		Le�r�s: Megadott koordint�n a Szagtalanito haszn�lata.
		x: A P�lya x koordin�t�ja
		y: A P�lya y koordin�t�ja
	 */
	public static boolean usesmell(String[] args) throws PearException {
		String err="Szagtalanito spray hasznalata nem sikerult.";
		try {
			boolean van=false;
			for (Eszkoz e: Controller.getTools()) {
				if (e.getProperty() == Property.SZAGTALAN) {
					van=true;
					if (Controller.getPalya().validPos(null,Integer.valueOf(args[0]), Integer.valueOf(args[1])) == null) throw new PearException("Szagtalanito spray hasznalata nem sikerult. A koordinatak a palyan kivul vannak.");
					else if (e.getCount()==0) throw new PearException("Szagtalanito spray mar kiurult");
					else {
						e.use(Integer.valueOf(args[0]), Integer.valueOf(args[1]));
						PearEventSource.fireUseSmellFinished(Integer.valueOf(args[0]), Integer.valueOf(args[1]), e.getCount());
						return true;
					}
				}
			}
			if (!van) throw new PearException("Szagtalanito spray meg nincs letrehozva");
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
		throw new PearException(err);
	}
	
	/*
	 * showMap <fileName>
		Le�r�s: A p�lya aktu�lis �llapot�t adja vissza mintha beg�pelt�k volna parancsok form�j�ban, �s egyben men�sre is szolg�l.
		fileName: Ha ezt megadjuk, a parancsok a f�jlba �r�dnak ki.
	 */
	public static boolean showmap(String args[]) throws PearException {
		PearEventSource.fireShowMapBegin();
		int oldStep=Controller.getPalya().getSteps();
		step(new String[]{"0"});
		String err="A palya mentese nem sikerult.";
		try {
			PrintStream out=System.out;
			boolean saveID=false;
			if ( args.length > 0 && !args[0].isEmpty() ) {
				if ( !"this".equals(args[0].toLowerCase()) )
					out=new PrintStream(args[0]);
			}
			if ( args.length > 1 ) {
				saveID=MyBoolean.valueOf(args[1]);
			}
			Controller.getPalya().setSaveID(saveID);
			Controller.save(out);
			if ( out != System.out ){
				out.close();
			}
			return true;
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
		finally {
			step(new String[]{""+oldStep});
			PearEventSource.fireShowMapFinished();
		}
	}
	
	/*
	 * addProperty <x> <y> <p>
		Le�r�s: Egy tulajdons�got �ll�tunk be egy adott poz�ci�n l�v� Elem-eken
		x: A P�lya x koordin�t�ja
		y: A P�lya y koordin�t�ja
		p: Az adott tulajdons�g, amit be akarunk �ll�tani
	 */
	public static boolean addproperty(String[] args) throws PearException {
		String err = "addProperty: "; //Kapunk mar uzenetetet a Palya-tol
		try {
			return Controller.getPalya().addProperty(Integer.valueOf(args[0]), Integer.valueOf(args[1]), Property.valueOf(args[2]));
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * setRandom <bool>
		Le�r�s: A v�letlen esem�nyek ki/be kapcsol�sa. Kikapcsol�s eset�n nem j�n l�tre mag�t�l hangy�szs�n, �s a mozg� szerepl�k a prefer�lt ir�nyt k�vetik.
		bool: true->V�letlenszer�s�g bekapcsolva, false->V�letlenszer�s�g kikapcsolva
	 */
	public static boolean setrandom(String[] args) throws PearException {
		String err="Veletlenszeruseg beallitasa nem sikerult.";
		try {
			return Controller.getPalya().setRandom(MyBoolean.valueOf(args[0]));
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * step <i>
		Le�r�s: A norm�l step kieg�sz�t�se
		i: H�nyszor l�pjen a j�t�k egym�s ut�n (automatikusan)
		Ez maga ut�n hordozza azt, hogy -1 -et megadva v�gtelen lesz!
	 */
	public static boolean step(String[] args) throws PearException {
		String err="Lepesek inditasa nem sikerult.";
		try {
			if (Integer.valueOf(args[0]) == 0) {
				PearEventSource.firePauseChanged(true);
			}
			else {
				PearEventSource.firePauseChanged(false);
			}
			return Controller.getPalya().setSteps(Integer.valueOf(args[0]));
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * allStep <i>
		Le�r�s: H�ny l�p�sn�l tart a p�lya
	 */
	public static boolean allstep(String[] args) throws PearException {
		String err="Lepesek beallitasa nem sikerult.";
		try {
			return Controller.getPalya().setAllSteps(Integer.valueOf(args[0]));
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * setDir <x> <y> <dir>
		Le�r�s: Manu�lis ir�nymegad�sra szolg�l. Az �llat ezent�l csak a megadott ir�nyba fog l�pni.
		x: Az �llat x koordin�t�ja a p�ly�n
		y: Az �llat y koordin�t�ja a p�ly�n
		dir: [b|j|bf|bl|jf|jl|auto]
		b: bal
		j: jobb
		bf: bal-fel
		bl: bal-le
		jf: jobb-fel
		jl: jobb-le
		auto: M.I. alapj�n
	 */
	public static boolean setdir(String[] args) throws PearException {
		String err="setDir: "; //Kapunk mar uzenetetet a Palya-tol
		try {
			return Controller.getPalya().setDir(Integer.valueOf(args[0]),Integer.valueOf(args[1]),Dir.valueOf(args[2]));
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	/*
	 * showCell <x> <y>
		Le�r�s: Ki�rja egy adott cella aktu�lis �llapot�t.
		y: A cella y koordin�t�ja
	 */
	public static boolean showcell(String[] args) throws PearException {
		String err="showCell: "; //Kapunk mar uzenetetet a Palya-tol
		try {
			return Controller.getPalya().showCell(Integer.valueOf(args[0]),Integer.valueOf(args[1]));
		} catch (Exception e) {
			throw new PearException(err+"\n"+PearException.stackTraceToString(e));
		}
	}
	
	public static void close(String[] args) throws PearException {
		throw new PearException("close");
}
	
	public static void exit(String[] args) throws PearException {
			throw new PearException("exit");
	}
}
