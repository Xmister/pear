package controller;

import java.awt.EventQueue;
import java.io.PrintStream;
import java.util.*;

import shared.PearException;
import shared.ErrorLevel;
import shared.PearConsoleHelper;
import shared.PearEvent;
import shared.PearEventAdapter;
import shared.PearEventSource;
import view.Form;
import view.PearConsole;
import view.PearView;

import model.Elem;
import model.Eszkoz;
import model.Palya;
import model.Property;


public final class Controller {

	private static PearEventAdapter pe = new PearEventAdapter() {
		@Override
		public void onMapCreated(PearEvent e, Palya p) {
			palya=p;
			for (PearView v : views ) {
				v.setPalya(p);
			}
			for (Eszkoz es: eszkozok) {
				es.setPalya(p);
			}
		}
		
		@Override
		public void onToolsCreated(PearEvent e, List<Eszkoz> esz) {
			eszkozok=esz;
		}
		
		@Override
		public void onClicked(PearEvent event, Elem elem) {
			if ( activeTool != null ) {
				useTool(activeTool, elem.getX(), elem.getY());
			}
		}
	};
	
	private static List<Eszkoz> eszkozok;
	private static Palya palya;
	private static List<PearView> views;
	private static Eszkoz activeTool;
	private static boolean autoPause=false;
	
	
	/*
	 * Itt indul a program.
	 * @param args: A program parameterei. Nincsenek feldolgozva.
	 */
	public static void main(String[] args) {
		views = new ArrayList<PearView>(2);
		PearEventSource.addEventListener(pe);
		PearEventSource.addEventListener(PearConsoleHelper.pe);
		eszkozok = new ArrayList<Eszkoz>();
		views.add(new PearConsole());
		views.add(new Form());
		for (final PearView v : views ) {
			PearEventSource.addEventListener(v.getPearEventAdapter());
			new Thread(v).start();
		}
		try {
			if (args.length > 0) {
				if ( args.length > 1 )
					Command.writecommands(new String[]{args[1]});
				load(args[0]); //Ha van param�ter, azt bet�ltj�k.
			}
		} catch ( PearException ce) {
			errorMessage(ErrorLevel.E_HIGH,ce.getMessage());
		}
	}
	
	public synchronized static void removeView(PearView view) {
		PearEventSource.removeEventListener(view.getPearEventAdapter());
		views.remove(view);
		if ( views.size() == 0 ) {
			PearEventSource.fireExit();
			exit();
		}
		else if ( views.size() == 1 ) views.get(0).setLogLevel(ErrorLevel.E_INFO); //Az utols� view mindenk�pp jelen�tsen meg minden hib�t.
	}
	
	public synchronized static void removeAllView() {
		for (int i=views.size()-1; i>=0; i--) {
			removeView(views.get(i));
		}
	}
	
	private static synchronized void exit() {
		System.exit(0);
	}

	public static Palya getPalya() {
		return palya;
	}
	
	public synchronized static void runOnEDT(Runnable r) {
		if ( !EventQueue.isDispatchThread() ) {
			EventQueue.invokeLater(r);
		} else r.run();
	}
	
	public synchronized static void runOnEDTAndWait(Runnable r) {
		if ( !EventQueue.isDispatchThread() ) {
			try {
				EventQueue.invokeAndWait(r);
			} catch (Exception e) { errorMessage(ErrorLevel.E_LOW,e.getMessage()); }
		} else r.run();
	}
	
	/*
	 * Letrehoz es elindit egy uj jatekot
	 */
	public synchronized static void newGame(int type) {
		int w, h, raktar, tocsa, ko, leso, cirto, rirto, cszag, rszag;
		//TODO: 0HIGH leso inicializ�l�sa
		leso = 2;
		switch (type){
			case 0: //Kicsi
				w=10;
				h=10;
				raktar=1;
				tocsa=3;
				ko=5;
				cirto=5;
				rirto=3;
				cszag=5;
				rszag=3;
			break;
			case 1: //K�zepes
			default:
				w=35;
				h=20;
				raktar=1;
				tocsa=3;
				ko=5;
				cirto=7;
				rirto=4;
				cszag=7;
				rszag=4;
			break;	
			case 2: //Nagy
				w=70;
				h=50;
				raktar=1;
				tocsa=3;
				ko=5;
				cirto=10;
				rirto=5;
				cszag=10;
				rszag=5;
			break;
			case 3: //�ri�si
				w=200;
				h=100;
				raktar=1;
				tocsa=3;
				ko=5;
				cirto=40;
				rirto=10;
				cszag=40;
				rszag=10;
			break;			
		}
		/*
		 * Palya inicializalasa
		 */
		try {
			/*
			 * �j p�lya l�trehoz�sa
			 */
			Command.newtrack(new String[]{String.valueOf(w), String.valueOf(h), String.valueOf(true)});
			/*
			 * Elemek gener�l�sa
			 */
			Command.generate(new String[]{String.valueOf(raktar),String.valueOf(tocsa),String.valueOf(ko),String.valueOf(leso)});
			/*
			 * Eszk�z�k hozz�ad�sa
			 */
			Command.addtools(new String[]{String.valueOf(cirto),String.valueOf(rirto),String.valueOf(cszag),String.valueOf(rszag)});
			/*
			 * Indul�s
			 */
			Command.step(new String[]{"-1"});
		} catch (PearException e) {
			String message=e.getMessage();
			message+="\nStackTrace:\n-----------";
			message+="\n"+PearException.stackTraceToString(e);
			Controller.errorMessage(ErrorLevel.E_HIGH,message);
		}
		
	}

	/*
	 * Lekezeli az eszkoz hasznalatat.
	 * @param e: A hasznalt eszkoz
	 * @param x: X koordinata
	 * @param y: Y koordinata
	 */
	public synchronized static void useTool(Eszkoz e, int x, int y) {
		try {
			if ( e.getProperty() == Property.MERGEZETT )
				Command.usepoison(new String[]{""+x,""+y});
			else if ( e.getProperty() == Property.SZAGTALAN )
				Command.usesmell(new String[]{""+x,""+y});
		} catch ( PearException pe ) {
			errorMessage(ErrorLevel.E_HIGH, pe.getMessage());
		}
		
	}
	
	/*
	 * Elmenti a jatek allasat
	 */
	public synchronized static void save(PrintStream out) {
		
		//Eszkozok
		if (eszkozok != null && eszkozok.size() > 0) {
			out.print("addTools ");
				//Mereg
				for (Eszkoz e: eszkozok ) {
					if ( e.getProperty() == Property.MERGEZETT) {
						out.print(e.getCount()+" ");
						out.print(e.getRadius()+" ");
						break;
					}
				}
				//Szagtalanito
				for (Eszkoz e: eszkozok ) {
					if ( e.getProperty() == Property.SZAGTALAN) {
						out.print(e.getCount()+" ");
						out.print(e.getRadius()+" ");
						break;
					}
				}
			out.println();
		}
		
		//Palya
		if (palya != null) {
			out.print("newTrack ");
				out.print(palya.getWidth()+" ");
				out.print(palya.getHeight()+" ");
				out.println("false");
				out.print(palya.getAll(!autoPause));
			out.println();
		}
		
	}
	
	/*
	 * Betolt egy jatekallast
	 * @param filename: Az elmentett allast tartalmazo fajl.
	 */
	public synchronized static boolean load(String filename) { 
		boolean ret=true;
		try {
			Command.readcommands(new String[]{filename});
		}
		catch (Exception e) {
			errorMessage(ErrorLevel.E_HIGH,e.getMessage());
			ret=false;
		}
		return ret;
	}
	
	public synchronized static void errorMessage(ErrorLevel level, String msg) {
		PearEventSource.fireError(level, msg);
	}
	
	
	
	/*
	 * Visszaadja az eszkozok listajat
	 */
	public static List<Eszkoz> getTools() {
		return eszkozok;
	}

	public static void setActiveTool(Property p) {
		if ( p != null ) {
			for ( Eszkoz e : eszkozok ) {
				if ( e.getProperty() == p ) {
					activeTool=e;
					break;
				}
			}
		} else activeTool = null;
	}
	
	public static void togglePause() throws PearException {
		autoPause=false;
		if ( palya != null ) {
			if (palya.getSteps() == 0){
				Command.step(new String[]{"-1"});
			}
			else {
				Command.step(new String[]{"0"});
			}
		}
	}
	
	public static void setPause(boolean p) throws PearException {
		if ( palya != null ) {
			if (p && palya.getSteps() != 0) {
				autoPause=true;
				Command.step(new String[]{"0"});
			}
			else if ( !p && palya.getSteps() == 0 ) {
				autoPause=true;
				Command.step(new String[]{"-1"});
			}
		}
	}
	
	public static boolean isPaused() {
		if ( palya != null ) {
			if (palya.getSteps() == 0) return true;
			return false;
		}
		return true;
	}
}
