package view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import shared.PearException;
import shared.ErrorLevel;
import shared.PearConsoleHelper;
import shared.PearEvent;
import shared.PearEventAdapter;

import model.Boly;
import model.Elem;
import model.Eszkoz;
import model.Hangya;
import model.Hangyaszsun;
import model.Palya;
import model.Property;
import model.Raktar;

import controller.Command;
import controller.Controller;

public class PearConsole implements PearView {
	private PearEventAdapter pe = new PearEventAdapter() {
		@Override
		public void onElemKilled(PearEvent ev, Elem e) {
			switch (e.getType()) {
				case HANGYA:
					PearConsoleHelper.I("Hangya elpusztult a(z) "+e.getX()+", "+e.getY()+" koordinatan.");
					break;
				case HANGYASZSUN:
					PearConsoleHelper.I("Hangyaszsun tavozott a palyarol.");
					break;
			}
		}
		
		@Override
		public void onElemAdded(PearEvent ev, Elem e) {
			switch (e.getType()) {
				case HANGYA:
					PearConsoleHelper.I("Hangya megjelent a(z) "+e.getX()+", "+e.getY()+" koordinatan.");
					break;
				case HANGYASZSUN:
					PearConsoleHelper.I("HangyaszSun megjelent a(z) "+e.getX()+", "+e.getY()+" koordinatan. Bendomeret: "+((Hangyaszsun)e).getAntCount()+"/"+((Hangyaszsun)e).getBellySize());
					break;
				default:
					PearConsoleHelper.I(e.getType().name()+" megjelent a(z) "+e.getX()+", "+e.getY()+" koordinatan.");
					break;
			}
		}
		
		@Override
		public void onElemMoved(PearEvent ev, Elem e, int newX, int newY) {
			switch (e.getType()) {
				case HANGYA:
					Hangya h=(Hangya)e;
					PearConsoleHelper.I("Hangya atlepett a(z) "+newX+", "+newY+" koordinatara. Carry: "+h.isCarry());
					break;
				case HANGYASZSUN:
					Hangyaszsun s=(Hangyaszsun)e;
					PearConsoleHelper.I("HangyaszSun atlepett a(z) "+newX+", "+newY+" koordinatara. Bendomeret: "+s.getAntCount()+"/"+s.getBellySize());
					break;
				default:
					PearConsoleHelper.I(e.getType().name()+" pozicioja megvaltozott: "+newX+", "+newY);
					break;
			}
		}
		
		@Override
		public void onToolsCreated(PearEvent e, List<Eszkoz> eszkozok) {
			PearConsoleHelper.I("Eszkozok hozzaadva.");
		}
		
		@Override
		public void onMapStep(PearEvent ev, int step) {
			PearConsoleHelper.I(step+". lepes:");
		}
		
		@Override
		public void onMapCreated(PearEvent e, Palya palya) {
			PearConsoleHelper.I("A Palya sikeresen letrehozva. Merete: "+palya.getWidth()+" * "+palya.getHeight()+".");
		}
		
		public void beforeExit(PearEvent e) {
			PearConsoleHelper.I("Kilepes. Viszlat!");
			loop=false;
		}

		@Override
		public void onReadCommandsFinished(PearEvent event, String fileName, boolean success) {
			if ( success )
				PearConsoleHelper.I("Betoltes a "+fileName+" nevu fajlbol sikerrel jart.");
		}
		
		@Override
		public void onGenerateFinished(PearEvent event, boolean success) {
			if ( success )
				PearConsoleHelper.I("A generalas sikerrel jart.");
		}

		@Override
		public void onWriteCommands(PearEvent event, String fileName) {
			PearConsoleHelper.I(fileName+" fajl letrehozva, es a kimenet atiranyitva.");
		}

		@Override
		public void onEndWriteCommands(PearEvent event) {
			PearConsoleHelper.I("Fajlba iras vege.");
		}

		@Override
		public void onUsePoisonFinished(PearEvent event, int x, int y, int count) {
			PearConsoleHelper.I("Hagyairto spray lef�jva a "+x+" ," +y+" koordinatakra.");
		}

		@Override
		public void onUseSmellFinished(PearEvent event, int x, int y, int count) {
			PearConsoleHelper.I("Szagtalanito spray lef�jva a "+x+" ," +y+" koordinatakra.");
		}

		@Override
		public void onShowMapFinished(PearEvent event) {
			PearConsoleHelper.I("A palya mentese sikerult.");
		}

		@Override
		public void onAddPropertyFinished(PearEvent event, Elem e, Property p) {
			PearConsoleHelper.I(e.getType().name()+": "+p.name()+" tulajdonsag beallitva.");
		}

		@Override
		public void onSetRandomFinished(PearEvent event, boolean random) {
			if (random) PearConsoleHelper.I("A veletlen esemenyek bekapcsolva.");
			else PearConsoleHelper.I("A veletlen esemenyek kikapcsolva.");
		}

		@Override
		public void onSetDirFinished(PearEvent event) {
			PearConsoleHelper.I("Preferalt irany beallitva.");
		}

		@Override
		public void onFoodDelivered(PearEvent event, Boly boly) {
			PearConsoleHelper.I("A Boly etelt vett fel. Elelemtartalma: "+boly.getDeliveredAmount());
		}

		@Override
		public void onShowCell(PearEvent event, String out) {
			PearConsoleHelper.I(out);
		}

		@Override
		public void onStorageChanged(PearEvent event, Raktar raktar) {
			PearConsoleHelper.I("Elelemraktar a(z) "+raktar.getX()+", "+raktar.getY()+" koordinatan csokkent. Kapacitasa: "+raktar.getCapacity()+"/"+raktar.getLeftAmmount());
		}
		
		@Override
		public void onError(PearEvent event, ErrorLevel level, String message) {
			if ( level.ordinal() >= loglevel )
				PearConsoleHelper.E(message);
		}
	};
	
	private boolean loop=true;
	private int loglevel=ErrorLevel.E_INFO.ordinal(); //A konzolra �rjunk ki minden hib�t
	
	
	public PearEventAdapter getPearEventAdapter() {
		return pe;
	}
	
	/*
	 * Parancsok v�grehajt�sa
	 * @param cmd: parancs
	 * @param args: param�terek
	 * @return true->j�het a k�vetkez� parancs, false->nem kell t�bb parancs.
	 */
	private static boolean command(String cmd, String[] args) {
		if (cmd.length() < 1) return true;
		try {
			PearConsoleHelper.setBeszedes(true);
			Command.class.getMethod(cmd.toLowerCase(), String[].class).invoke(null, (Object)args);
			PearConsoleHelper.setBeszedes(false);
			return true;
		} 
		catch (InvocationTargetException ie) {
			if ( ie.getCause() instanceof PearException ) {
				PearException ce = (PearException) ie.getCause();
				if (ce.getMessage().equals("close")) {
					return false;
				}
				else if (ce.getMessage().equals("exit")) {
					Controller.removeAllView();
					return false;
				}
				Controller.errorMessage(ErrorLevel.E_LOW, ce.getMessage());
			} else {
				Exception e = (Exception) ie;
				String message=e.getMessage();
				message+=("Reszletes hibauzenet:\n");
				message+=PearException.stackTraceToString(e);
				message+=("Bocsanat! A parancsertelmezo leall."); //Egy kis Android utalas.
				Controller.errorMessage(ErrorLevel.E_LOW, message);
			}
			return true;
		}
		catch (NoSuchMethodException ne) {
			Controller.errorMessage(ErrorLevel.E_INFO, "Mi ez? "+cmd+" parancs nincs.");
			return true;
		}
		catch (Exception e) {
			String message=e.getMessage();
			message+=("Reszletes hibauzenet:\n");
			message+=PearException.stackTraceToString(e);
			message+=("Bocsanat! A parancsertelmezo leall."); //Egy kis Android utalas.
			Controller.errorMessage(ErrorLevel.E_LOW, message);
			return false;
		}
	}
	
	public void inloop() {
		boolean ret=true;
		try {
			PearConsoleHelper.I("Parancsertelmezo elindult. Have fun!");
			while (loop && ret) {
				String line=new BufferedReader(new InputStreamReader(System.in)).readLine();
				String[] tokens=line.split(" ");
				String cmd=tokens[0];
				String[] args = Arrays.copyOfRange(tokens, 1, tokens.length);
				PearConsoleHelper.setBeszedes(true);
				ret=command(cmd,args);
				PearConsoleHelper.setBeszedes(false);
			}
		} catch (IOException e) {
			String message=e.getMessage();
			message+=("Reszletes hibauzenet:\n");
			message+=PearException.stackTraceToString(e);
			message+=("Bocsanat! A parancsertelmezo leall."); //Egy kis Android utalas.
			Controller.errorMessage(ErrorLevel.E_LOW, message);
		}
		Controller.removeView(this);
	}

	@Override
	public void setPalya(Palya palya) {

	}

	@Override
	public void run() {
		inloop();
	}

	@Override
	public void setLogLevel(ErrorLevel level) {
		loglevel=level.ordinal();
	}
}
