package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import shared.PearException;
import shared.ErrorLevel;
import shared.PearEvent;
import shared.PearEventAdapter;

import controller.Command;
import controller.Controller;

import model.Boly;
import model.Eszkoz;
import model.Hangyaszsun;
import model.Palya;
import model.Elem;
import model.Property;


public class Form extends JFrame implements PearView {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5118528236147700416L;
	/*
	 * Form Helper oszt�lyok
	 */
	/*
	 * Ablak esem�nyek
	 */
	private WindowAdapter wl = new WindowAdapter() {
		@Override
		public void windowClosing(WindowEvent e) {
			closed=true;
			Controller.removeAllView();
		}
	};
	/*
	 * Komponens esem�nyek
	 */
	private ComponentAdapter ca = new ComponentAdapter() {
		@Override
		public void componentResized(ComponentEvent e) {
			onResize();
		}
	};
	/*
	 * Billenty�zet esem�nyek
	 */
	private AbstractAction faster = new AbstractAction() {
        /**
		 * 
		 */
		private static final long serialVersionUID = 3388503275930754274L;

		@Override
        public void actionPerformed( ActionEvent e ) {
        	if ( palya != null ) palya.faster(); 
        }
    };
    private AbstractAction slower = new AbstractAction() {
		/**
		 * 
		 */
		private static final long serialVersionUID = -3073078324420567347L;

		@Override
        public void actionPerformed( ActionEvent e ) {
        	if ( palya != null ) palya.slower();
        }
    };
	/*
	 * Saj�t esem�nyeink
	 */
	private PearEventAdapter pe = new PearEventAdapter() {
		@Override
		public void onCellChanged(PearEvent e, final int x, final int y) {
			if ( !display ) return;
			if ( palya != null ) {
				final List<Elem> cell = palya.getElems(x, y, 1).get(0);
				if ( addCell(cell) )
					Controller.runOnEDT(new Runnable() { //Csak UI sz�lon futhat
						public void run() {
							elemek[x][y].repaint();
						}
					});
			}
		}
		
		@Override
		public void beforeExit(PearEvent e) {
			Controller.runOnEDT(new Runnable() {
				public void run() {
					setVisible(false);
					removeMapVisualBackend();
				}
			});
		}
		
		@Override
		public void onError(PearEvent event, ErrorLevel level, final String message) {
			if ( level.ordinal() >= loglevel )
				Controller.runOnEDT(new Runnable() { //Csak UI sz�lon futhat
					public void run() {
						JOptionPane.showMessageDialog(Form.this, message, "Hiba!", JOptionPane.ERROR_MESSAGE);
					}
				});
		}
		
		@Override
		public void onReadCommandsBegin(PearEvent event, String fileName) {
			Controller.runOnEDT(new Runnable() { //Csak UI sz�lon futhat
					public void run() {
						startProgress("P�lya bet�lt�se folyamatban...");
					}
				});
		}
		
		@Override
		public void onShowMapBegin(PearEvent event) {
			Controller.runOnEDT(new Runnable() { //Csak UI sz�lon futhat
				public void run() {
					startProgress("P�lya ment�se folyamatban...");
				}
			});
		}
		
		@Override
		public void onShowMapFinished(PearEvent event) {
			Controller.runOnEDT(new Runnable() { //Csak UI sz�lon futhat
				public void run() {
					endProgress();
				}
			});
		}
		
		@Override
		public void onReadCommandsFinished(PearEvent event, String fileName, boolean success) {
			if ( !success )
				JOptionPane.showMessageDialog(Form.this, fileName+" bet�lt�se nem siker�lt.", "Hiba!", JOptionPane.ERROR_MESSAGE);
				Controller.runOnEDT(new Runnable() { //Csak UI sz�lon futhat
					public void run() {
						onResize();
						endProgress();
					}
				});
		}
		
		@Override
		public void onGameOver(PearEvent event, final int steps) {
			//TODO: 1MED: Pontrendszer. Min�l t�bb l�p�s, ann�l �gyesebb volt valaki.
			Controller.runOnEDT(new Runnable() {
				public void run() {
					String nev=JOptionPane.showInputDialog(Form.this, "Siker�lt "+steps+" l�p�sig h�znod, grat!\nMost add meg a neved, k�rlek!", "J�t�k v�ge!", JOptionPane.INFORMATION_MESSAGE);
					addHighScore(steps, nev);
				}
			});
		}
		
		@Override
		public void onMapStep(PearEvent ev, final int step) {
			Controller.runOnEDT(new Runnable() {
				public void run() {
					lSteps.setText(""+step);
				}
			});
		}
		
		@Override
		public void onSunAte(PearEvent event, final Hangyaszsun hangyaszsun) {
			Controller.runOnEDT(new Runnable() {
				public void run() {
					lSunBendo.setText(hangyaszsun.getAntCount()+"/"+hangyaszsun.getBellySize());
				}
			});
		}
		
		@Override
		public void onElemKilled(PearEvent ev, final Elem e) {
			Controller.runOnEDT(new Runnable() {
				public void run() {
					if (e.getType() == model.Type.HANGYASZSUN) lSunBendo.setText("-");
				}
			});
		}
		
		
		@Override
		public void onToolsCreated(PearEvent e, final List<Eszkoz> eszkozok) {
			Controller.runOnEDT(new Runnable() {
				public void run() {
					/*
					 * Eszk�z vez�rl�k hozz�ad�sa
					 */
					if ( toolBar != null ) {
						if ( szagt != null ) toolBar.remove(szagt);
						if ( hirto != null ) toolBar.remove(hirto);
						szagt = new JButton("Szagtalan�t�");
						szagt.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent arg0) {
								if ( hirto.getBackground() == Color.GREEN ) hirto.setBackground(UIManager.getColor("JButton.background")); //A m�sikat mindenk�pp "deaktiv�ljuk"
								if ( szagt.getBackground() == Color.GREEN ) {
									Controller.setActiveTool(null);
									szagt.setBackground(UIManager.getColor("JButton.background")); //Default vissza�ll�t�sa
								} else if ( szagt.getBackground() == Color.RED ) {
									//Elfogyott
								} else {
									Controller.setActiveTool(Property.SZAGTALAN);
									szagt.setBackground(Color.GREEN);
								}
							}
						});
						toolBar.add(szagt);
						hirto = new JButton("Hangya�rt�");
						hirto.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent arg0) {
								if ( szagt.getBackground() == Color.GREEN ) szagt.setBackground(UIManager.getColor("JButton.background")); //A m�sikat mindenk�pp "deaktiv�ljuk"
								if ( hirto.getBackground() == Color.GREEN ) {
									Controller.setActiveTool(null);
									hirto.setBackground(UIManager.getColor("Jhirto.background")); //Default vissza�ll�t�sa
								} else if ( hirto.getBackground() == Color.RED ) {
									//Elfogyott
								} else {
									Controller.setActiveTool(Property.MERGEZETT);
									hirto.setBackground(Color.GREEN);
								}
							}
						});
						toolBar.add(hirto);
					}
					/*
					 * Feliratok �t�r�sa
					 */
					for (Eszkoz es: eszkozok) {
						switch ( es.getProperty()) {
							case MERGEZETT:
								hirto.setText("Hangya�rt� ("+es.getCount()+")");
								break;
							case SZAGTALAN:
								szagt.setText("Szagtalan�t� ("+es.getCount()+")");
								break;
						}
					}
				}
			});
		}
		
		@Override
		public void onAddAntEaterFinished(PearEvent event, final Hangyaszsun s) {
			Controller.runOnEDT(new Runnable() {
				public void run() {
					lSunBendo.setText(s.getAntCount()+"/"+s.getBellySize());
				}
			});
		}
		
		@Override
		public void onUsePoisonFinished(PearEvent event, int x, int y, final int count) {
			Controller.runOnEDT(new Runnable() {
				public void run() {
					hirto.setText("Hangya�rt� ("+count+")");
					if ( count == 0 ) {
						hirto.setBackground(Color.RED);
						Controller.setActiveTool(null);
						JOptionPane.showMessageDialog(Form.this, "A hangya�rt� spray ki�r�lt!", "Ki�r�lt az eszk�z!", JOptionPane.INFORMATION_MESSAGE);
					}
				}
			});
		}

		@Override
		public void onUseSmellFinished(PearEvent event, int x, int y, final int count) {
			Controller.runOnEDT(new Runnable() {
				public void run() {
					szagt.setText("Szagtalan�t� ("+count+")");
					if ( count == 0 ) {
						szagt.setBackground(Color.RED);
						Controller.setActiveTool(null);
						JOptionPane.showMessageDialog(Form.this, "A szagtalan�t� spray ki�r�lt!", "Ki�r�lt az eszk�z!", JOptionPane.INFORMATION_MESSAGE);
					}
				}
			});
		}
		
		@Override
		public void onFoodDelivered(PearEvent event, final Boly boly) {
			Controller.runOnEDT(new Runnable() {
				public void run() {
					lBoly.setText(""+boly.getDeliveredAmount());
				}
			});
		}
		
		@Override
		public void onElemAdded(PearEvent ev, final Elem e) {
			switch (e.getType()) {
				case BOLY:
					Controller.runOnEDT(new Runnable() {
						public void run() {
							lBoly.setText("0");
						}
					});
				break;
			}
		}
		
		@Override
		public void onFoodCountChanged(PearEvent event, final int foodCount) {
			Controller.runOnEDT(new Runnable() {
				public void run() {
					lFood.setText(""+foodCount);
				}
			});
		}
		
		@Override
		public void onSpeedChange(PearEvent event, long timer_step) {
			if ( timer_step < 20 ) { //20ms
				Controller.runOnEDT(new Runnable() {
					public void run() {
						if ( display ) {
							display=false;
							JOptionPane.showMessageDialog(Form.this, "A l�p�s t�l gyors a megjelen�t�shez! Innent�l kezdve szimul�ci� folyik.", "T�l gyors!", JOptionPane.INFORMATION_MESSAGE);
						}
					}
				});
			}
			else {
				Controller.runOnEDT(new Runnable() {
					public void run() {
						if ( !display ) {
							display=true;
							JOptionPane.showMessageDialog(Form.this, "�jra mutatjuk a t�rt�n�seket.", "A sebess�g megfigyelhet�", JOptionPane.INFORMATION_MESSAGE);
						}
					}
				});
			}
		}
		
		@Override
		public void onPauseChange(PearEvent event, final boolean p) {
			Controller.runOnEDT(new Runnable() {
				public void run() {
					if ( p ) {
						pause.setText("Folytat�s");
					} else {
						pause.setText("Sz�net");
					}
				}
			});
		}

	};
	/*
	 * V�ltoz�k
	 */
	private JPanel ePanel;
	private JScrollPane sPane;
	private JToolBar toolBar;
	private JButton hirto;
	private JButton szagt;
	private JButton pause;
	private boolean closed;
	private Palya palya=null;
	private VisibleElem[][] elemek=null;
	private final int minRatio = 10; //Enn�l kisebb ne legyen egy hatsz�g
	private int ratio = 31; //Pixel/Poz�ci� ar�ny
	private int loglevel = ErrorLevel.E_HIGH.ordinal();
	private JProgressBar pb;
	private JPanel statusBar;
	private JLabel lSunBendo;
	private JLabel lSteps;
	private JLabel lBoly, lFood;
	private boolean display=true;
	
	/*
	 * Met�dusok
	 */
	private void init() {
	    /*
	     * Layout be�ll�t�sa
	     */
		setLayout(new BorderLayout());
		/*
		 * Men� l�trehoz�sa
		 */
		JMenuBar menuBar;
		JMenu menu;
		JMenuItem menuItem;
		menuBar = new JMenuBar();
		menu = new JMenu("Men�");
		menuBar.add(menu);
		menuItem = new JMenuItem("�j j�t�k");
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				//... Text to put on the buttons.
		        String[] choices = {"Kicsi", "K�zepes", "Nagy", "�ri�si", "M�gse"};
				final int response=JOptionPane.showOptionDialog(Form.this, "P�lya m�rete", "M�ret", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, choices, "K�zepes");
				if ( response < choices.length-1 ) {
					new Thread(new Runnable() {
						public void run() {
							Controller.newGame(response);
						}
					}).start();
				}
			}
			
		});
		menu.add(menuItem);
		menuItem = new JMenuItem("Bet�lt�s");
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception e) {}
				final JFileChooser fc = new JFileChooser(new File(Form.class.getProtectionDomain().getCodeSource().getLocation().getPath())); //A saj�t mapp�nkban induljunk
				try {
					UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
				} catch (Exception e) {}
				fc.setName("J�t�k bet�lt�se f�jlb�l");
				fc.setFileFilter(new FileNameExtensionFilter("DAT", "dat"));
				int result = fc.showOpenDialog(Form.this);
				//opening
				if (result == JFileChooser.APPROVE_OPTION) {
					new Thread(new Runnable() { //Ne az UI sz�lat fogjuk ezzel.
						public void run() {
								String file = fc.getSelectedFile().getAbsolutePath();
								try {
									Command.readcommands(new String[]{file});
								} catch (PearException e) {
									String message=e.getMessage();
									message+="\nStackTrace:\n-----------";
									message+="\n"+PearException.stackTraceToString(e);
									Controller.errorMessage(ErrorLevel.E_HIGH,message);
								}
					}}).start();
				}
			}
			
		});
		menu.add(menuItem);
		menuItem = new JMenuItem("Ment�s");
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				final boolean p=Controller.isPaused();
				try {
					Controller.setPause(true);
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception e) {}
				final JFileChooser fc = new JFileChooser(new File(Form.class.getProtectionDomain().getCodeSource().getLocation().getPath())); //A saj�t mapp�nkban induljunk
				try {
					UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
				} catch (Exception e) {}
				fc.setName("J�t�k ment�se f�jlba");
				fc.setFileFilter(new FileNameExtensionFilter("DAT", "dat"));
				int result = fc.showSaveDialog(Form.this);
				//opening
				if (result == JFileChooser.APPROVE_OPTION) {
					new Thread(new Runnable() {
						public void run() {
								String file = fc.getSelectedFile().getAbsolutePath();
								//append extension
								if (!file.toLowerCase().endsWith(".dat")) {
									file = file + ".dat";
								}
								try {
									Command.showmap(new String[]{file, "true"});
								} catch (PearException e) {
									String message=e.getMessage();
									message+="\nStackTrace:\n-----------";
									message+="\n"+PearException.stackTraceToString(e);
									Controller.errorMessage(ErrorLevel.E_HIGH,message);
								}
					}}).start();
				}
				try {
					Controller.setPause(p);
				} catch (Exception e) {}
			}
			
		});
		menu.add(menuItem);
		menuItem = new JMenuItem("Pontt�bla");
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				SortedMap<Integer, String> al = getHighScores();
				String res="";
				if ( al != null ) {
					for (Entry<Integer, String> e: al.entrySet() ) {
						res+=e.getValue()+": "+e.getKey()+"\n";
					}
					JOptionPane.showMessageDialog(Form.this, res, "Pontt�bla", JOptionPane.INFORMATION_MESSAGE);
				}
			}
			
		});
		menu.add(menuItem);
		menu.addSeparator();
		menuItem = new JMenuItem("Ablak bez�r�sa");
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				closed=true;
				Controller.removeView(Form.this);
				setVisible(false);
			}
			
		});
		menu.add(menuItem);
		menuItem = new JMenuItem("Kil�p�s");
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				closed=true;
				Controller.removeAllView();
			}
			
		});
		menu.add(menuItem);
		setJMenuBar(menuBar);
		/*
		 * Toolbar l�trehoz�sa
		 */
		toolBar = new JToolBar();
		toolBar.setFloatable(false);
		add(toolBar,BorderLayout.PAGE_START);
		/*
		 * StatusBar l�trehoz�sa
		 */
		statusBar = new JPanel(new BorderLayout());
		JPanel stepPanel=new JPanel();
		stepPanel.add(new JLabel("P�lya l�p�sek: "));
		lSteps = new JLabel("-");
		stepPanel.add(lSteps);
		statusBar.add(stepPanel,BorderLayout.WEST);
		JPanel sunPanel=new JPanel();
		sunPanel.add(new JLabel("Hangy�szs�n bend�: "));
		lSunBendo = new JLabel("-");
		sunPanel.add(lSunBendo);
		statusBar.add(sunPanel,BorderLayout.EAST);
		JPanel gamePanel=new JPanel();
		gamePanel.add(new JLabel("J�t�k �ll�sa: "));
		lBoly= new JLabel("-");
		gamePanel.add(lBoly);
		gamePanel.add(new JLabel("/"));
		lFood= new JLabel("-");
		gamePanel.add(lFood);
		statusBar.add(gamePanel,BorderLayout.CENTER);
		add(statusBar,BorderLayout.SOUTH);
		/*
		 * Esem�nykezel�k hozz�ad�sa
		 */
		addComponentListener(ca);
		addWindowListener(wl);
		KeyStroke f = KeyStroke.getKeyStroke("F");
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(f, "faster");
        getRootPane().getActionMap().put("faster", faster); //F-re gyors�tunk
        KeyStroke s = KeyStroke.getKeyStroke("S");
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(s, "slower");
        getRootPane().getActionMap().put("slower", slower); //S-re lass�tunk
	}
	
	public SortedMap<Integer,String> getHighScores() {
		String dir=".";
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(dir+"/scores.dat"));
			Object obj = in.readObject();
			in.close();
			if ( obj instanceof SortedMap ) {
				@SuppressWarnings("unchecked")
				SortedMap<Integer,String> al = (SortedMap<Integer,String>)obj;
				return al;
			}
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	public void addHighScore(Integer hs, String name) {
		String dir=".";
		try {
			SortedMap<Integer, String> scores =getHighScores();
			if ( scores == null ) scores = new TreeMap<Integer, String>(java.util.Collections.reverseOrder());
			scores.put(hs, name);
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(dir+"/scores.dat"));
			out.writeObject(scores);
			out.close();
		} catch (Exception e) {
		}
	}
	
	public Form() {
		init();
		closed=false;
	}
	
	private void startProgress(String message) {
		if ( pb == null ) {
			pb = new JProgressBar();
			pb.setIndeterminate(true);
			pb.setString(message);
			pb.setStringPainted(true);
			toolBar.add(pb);
			pb.setVisible(true);
			toolBar.updateUI();
			setEnabled(false);
		}
	}
	
	private void endProgress() {
		if ( pb != null ) {
			pb.setVisible(false);
			toolBar.remove(pb);
			pb = null;
		}
		setEnabled(true);
	}
	
	private void addMapVisualBackend() {
		removeMapVisualBackend();
		/*
		 * Panel �s ScrollPane l�trehoz�sa
		 */		
		ePanel = new JPanel();
		ePanel.setLayout(null);
		ePanel.setDoubleBuffered(true);
		sPane = new JScrollPane(ePanel,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		add(sPane,BorderLayout.CENTER);
		/*
		 * Pause gomb l�trehoz�sa
		 */
		pause = new JButton("Sz�net");
		pause.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					Controller.togglePause();
				} catch (Exception e) {}
			}
		});
		toolBar.add(pause);
	}
	
	private void removeMapVisualBackend() {
		if ( ePanel != null) {
			ePanel.removeAll();
			sPane.remove(ePanel);
			ePanel = null;
		}
		if ( sPane != null) {
			sPane.removeAll();
			remove(sPane);
			sPane = null;
		}
		if ( toolBar != null ) {
			if ( pause != null ) {
				toolBar.remove(pause);
				pause=null;
			}
		}
		palya=null;
	}
	
	public void onResize() { //A ratio-t az ablak �j m�ret�nek megfelel�en �ll�tjuk �t, hogy min�l jobban belef�rj�nk.
			if ( palya != null ) { //Sok elemn�l el�frodulhat, hogy el�bb jutunk el ide, mint v�gezn�nk azok hozz�ad�s�val.
				Dimension dim=computeDimension();
				if ( dim.getWidth() < ePanel.getSize().getWidth() || dim.getHeight() < ePanel.getSize().getHeight() ) do { //N�veksz�nk
					ratio++;
					dim =computeDimension();
				} while (dim.getWidth() < ePanel.getSize().getWidth() || dim.getHeight() < ePanel.getSize().getHeight());
				if ( dim.getWidth() > ePanel.getSize().getWidth() || dim.getHeight() > ePanel.getSize().getHeight() ) do { //Cs�kken�nk
					ratio--;
					dim =computeDimension();
				} while (dim.getWidth() > ePanel.getSize().getWidth() || dim.getHeight() > ePanel.getSize().getHeight());
				ratio=Math.max(ratio, minRatio); //A megadott minRatio-n�l nem lehet�nk kisebbek.
				VisibleElem.setRatio(ratio);
				VisibleElem.rePosShared(); //A k�z�s sz�m�t�sokat elv�gezz�k egyszerre.
				for ( VisibleElem[] row : elemek) {
					for ( VisibleElem el : row) {
						el.rePos();
					}
				}
			}
	}
	
	public boolean isClosed() {
		return closed;
	}
	
	/*
	 * Kisz�molja, hogy a jelenlegi ratio-val mekkora panel kell, hogy kif�rjen az �sszes VisibleElem.
	 */
	private Dimension computeDimension() {
		double xoffset=VisibleElem.xoffset, xoffsetpercell=VisibleElem.xoffsetpercell, yoffsetpercell=VisibleElem.yoffsetpercell;
		yoffsetpercell=-((VisibleElem._actualHeight-VisibleElem._innerHeight)*ratio)/2+2; //+2: 2 pixel r�s a sorok k�z�tt
		return new Dimension((int)(palya.getWidth()*(VisibleElem._innerWidth*ratio+xoffsetpercell)+xoffset+(VisibleElem._innerWidth*ratio)/2),(int)(palya.getHeight()*(VisibleElem._actualHeight*ratio+yoffsetpercell)+((VisibleElem._actualHeight-VisibleElem._innerHeight)*ratio)/2));
		
	}
	
	
	public PearEventAdapter getPearEventAdapter() {
		return pe;
	}

	public synchronized void setPalya(final Palya palya) {
		/*
		 * Tudom ,nem t�l sz�p ez a sok EventQueue, de csak �gy lehet megoldani, hogy legyen vizu�lis visszajelz�s an�lk�l, hogy megfogn�nk az UI sz�lat a sz�mol�sokkal is.
		 */
			Controller.runOnEDTAndWait(new Runnable() { //Csak UI sz�lon futhat
				public void run() {
					removeMapVisualBackend();
				}
			});
		if ( palya != null) { //null palya -> Itt meg�l�lunk -> Vizu�lis p�lya t�rl�se
				Controller.runOnEDTAndWait(new Runnable() {
					public void run() {
						addMapVisualBackend();
					}
				});
			final boolean reportProgress = (palya.getWidth()*palya.getHeight() > 5000);
			final boolean hadPb = (pb != null);
			final JProgressBar pb = new JProgressBar(0, palya.getWidth()*palya.getHeight());
				Controller.runOnEDTAndWait(new Runnable() {
					public void run() {
						if ( !hadPb) setEnabled(false); //Jelenleg minden felhaszn�l�i beavatkoz�s, csak feleslegesen terheli az EventQueue-t, ami sok komponens eset�n egy�ltal�n nem j� dolog.
						if (reportProgress) { //Ha �gy van, akkor csin�ljunk egy ProgressBar-t neki
							pb.setValue(1);
							pb.setVisible(true);
							pb.setStringPainted(true);
							toolBar.add(pb);
							toolBar.updateUI();
						}
						ePanel.setVisible(false); //Jobb teljes�tm�ny
						sPane.setVisible(false);
					}
				});
			elemek = new VisibleElem[palya.getWidth()][palya.getHeight()];
			int minPosRatio; //Az adott poz�ci� feliratok mekkora minim�lis Ratio-val f�rnek ki
			if ( palya.getWidth() > 1000 || palya.getHeight() > 1000 )
				minPosRatio=55;
			else if ( palya.getWidth() > 100 || palya.getHeight() > 100 )
				minPosRatio=35;
			else if ( palya.getWidth() > 10 || palya.getHeight() > 10 )
				minPosRatio=25;
			else
				minPosRatio=20;
			int i=0;
			VisibleElem.setMinPosRatio(minPosRatio);
			VisibleElem.setRatio(ratio);
			VisibleElem.rePosShared(); //A k�z�s sz�m�t�sokat elv�gezz�k egyszerre.
			for ( final List<Elem> cell : palya.getElems() ) {
				if ( reportProgress && i%25==0) { //Ha visszajelz�nk, 25 elemenk�nt friss�ts�k a progressBar-t
					final int safei = i;
						Controller.runOnEDTAndWait(new Runnable() {
							public void run() {
								pb.setValue(safei);
								pb.setString("Bet�lt�s..."+pb.getValue()+"/"+pb.getMaximum());
							}
						});
				}
				addCell(cell);
				i++;
			}
			if ( reportProgress ) {
					Controller.runOnEDTAndWait(new Runnable() {
						public void run() {
							pb.setVisible(false);
							toolBar.remove(pb);
							toolBar.updateUI();
						}
					});
			}
				Controller.runOnEDTAndWait(new Runnable() {
					public void run() {
						Form.this.palya = palya; //Ezzel ellen�r�zhet�, hogy a bet�lt�s v�g�re jutottunk-e.

						if ( !hadPb ) setEnabled(true);
						ePanel.setVisible(true);
						sPane.setVisible(true);
						int tempRatio=ratio;
						ratio=minRatio;
						Dimension minSize=computeDimension();
						ratio=tempRatio;
						ePanel.setPreferredSize(minSize); //A bels� panel m�ret�nek minimuma a ScrollPane-ben az elemek legkisebb ar�ny� kirajzol�s�val el�rhet� m�ret 
						sPane.updateUI();
						validate(); //�jrasz�molja az sPane m�ret�t a Form-hoz viszony�tva
						onResize();
					}
				});
		}
	}
	
	/*
	 * cell cella megjelen�t�se a View-on.
	 * return -> true, ha siker�lt
	 */
	private boolean addCell(List<Elem> cell) {
		int x=0,y=0;
		if ( cell == null || cell.isEmpty() ) return false;
		x=cell.get(0).getX();
		y=cell.get(0).getY();
		if ( elemek.length > x && elemek[x].length > y && elemek[x][y] != null ) { 
			Elem el=cell.get(cell.size()-1);
			if ( cell.size() > 2 ) {
				for (Elem e: cell) {
					if ( e.getType() == model.Type.HANGYASZSUN ) {  //Ha Hangya �s Hangy�szs�n is van, a s�nt mutassuk.
						el = e;
						break;
					}
				}
			}
			elemek[x][y].setElem(el);
		} else {
			Elem el=cell.get(cell.size()-1);//A legfels� elem l�tszik
			if ( cell.size() > 2 ) {
				for (Elem e: cell) {
					if ( e.getType() == model.Type.HANGYASZSUN ) {  //Ha Hangya �s Hangy�szs�n is van, a s�nt mutassuk.
						el = e;
						break;
					}
				}
			}
			final VisibleElem ve = new VisibleElem(el); 
			elemek[x][y]=ve;
			ve.rePos();
				Controller.runOnEDTAndWait(new Runnable() { //Csak UI sz�l.
					public void run() {
						ePanel.add(ve);
						ve.updateUI();
					}
				});
				
		}
		return true;
	}

	@Override
	public void run() {
		/*
		 * Form els�dleges inicializ�l�sa
		 */
		setTitle("Hangyafarm - Pear�");
		setSize(500, 500);
		setVisible(true);
	}

	@Override
	public void setLogLevel(ErrorLevel level) {
		loglevel=level.ordinal();
	}

}
