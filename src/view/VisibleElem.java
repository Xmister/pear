package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;

import shared.PearEventSource;

import model.Elem;
import model.Property;
import model.Type;

public class VisibleElem extends JComponent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3602186110111360848L;
	/*
	 * Esem�nykezel� oszt�ly
	 */
	MouseAdapter ml = new MouseAdapter() {
				
		@Override
		public void mouseClicked(MouseEvent e) {
			if (elem != null)
				PearEventSource.fireClicked(elem);
		}
	};
	
	/*
	 * Osztott attrib�tumok. Teljes�tm�ny szempontj�b�l pozit�v. Min�l t�bb osztott attrib�tum, ann�l kevesebb mem�ria �s sz�mol�s.
	 */
	public static final double _innerWidth=1, _innerHeight=1, _actualHeight=2; //M�retek _ar�nya_
	public static int innerWidth, innerHeight, actualHeight; //A m�retek pixelben
	private static int ratio=10, minPosRatio=20; //Pixel/poz�ci� ar�ny, minPosRatio: Enn�l kisebb ratio eset�n nem �runk poz�ci�t.
	private static int[] _lRX, _lRY, _rRX, _rRY, textPos=new int[2];
	private static int fontSize=3;
	public static double xoffset=2, yoffset=2, xoffsetpercell=1, yoffsetpercell=1; // 4 pixel r�s az oszlopok k�z�tt, �s 2 pixel a container sz�l�t�l
	private static double evenxoffset;
	/*
	 * Egyedi attrib�tumok.
	 */
	private Elem elem;
	private Color color, secondColor, textColor;
	private String posText;
	private int[] posPos=new int[2];
	
	public VisibleElem(Elem e) {
		setElem(e);
		rePos();
		addMouseListener(ml);
		setDoubleBuffered(true);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(color);
	    g.fillPolygon(_lRX, _lRY, 4);
	    g.setColor(secondColor);
	    g.fillPolygon(_rRX, _rRY, 4);
	    g.setColor(textColor);
	    if ( ratio >= 10 ) { //K�l�nben kil�gn�nak a bet�k
		    g.setFont(new Font(Font.SANS_SERIF,Font.PLAIN,fontSize));
		    g.drawString(elem.getShortName(), textPos[0], textPos[1]);
		    if ( ratio >= minPosRatio ) { //K�l�nben kil�gna a poz�ci�
			    g.drawString(posText, posPos[0], posPos[1]);
		    }
	    }
	}
	
	private Color getComplement(Color c) {
		if (c.getBlue() == c.getGreen() && c.getGreen() == c.getRed()) {
			if (c.getBlue()<128)
				return new Color (255,255,255);
			else
				return new Color (0,0,0);
		}
		return new Color(	255-c.getRed(),
                			255-c.getGreen(),
                			255-c.getBlue()
                		);
	}
	
	public int getEX() {
		return elem.getX();
	}
	
	public void setEX(int x) {
		elem.setX(x);
	}
	
	public int getEY() {
		return elem.getY();
	}
	
	public void setEY(int y) {
		elem.setX(y);
	}

	public int getRatio() {
		return ratio;
	}

	public static void setRatio(int ratio) {
		VisibleElem.ratio = ratio;
	}
	
	public static void setMinPosRatio(int minPosRatio) {
		VisibleElem.minPosRatio = minPosRatio;
	}

	public void rePos() { //Igaz�b�l mi nem mozgunk, �gy ez csak a konstruktorban, vagy explicit h�v�dik meg, pl. �tm�retez�skor. Az itt tal�lhat� sz�mol�sok ez�rt "�r�k �rv�ny�ek", am�g nem kapunk �j ratio-t, nem kell �ket �jra sz�molni.
		double xoffset=VisibleElem.xoffset, yoffset=0;
		if ( getEY()%2 == 0 ) xoffset = evenxoffset;
		if ( getEY() > 0 ) yoffset = VisibleElem.yoffset; //Az els� sort m�g nem kell feljebb tolni
		setBounds((int)(getEX()*(innerWidth)+xoffset), (int)(getEY()*(actualHeight+yoffset)), innerWidth, actualHeight);
		/*
	     * Poz�ci� felirat be�ll�t�sa
	     */
	    posText=getEX()+","+getEY();
	    //V�zszintesen kb. k�z�pre igaz�tjuk
	    posPos[0]=(int)((innerWidth)/2-posText.length()*2.5);
	    //F�gg�legesen pedig mindig alulra. Csak akkor jelenik majd meg, ha el is f�r.
	    posPos[1]=textPos[1]+(innerHeight)/2;
	}
	
	public static void rePosShared() {
		/*
		 * M�retek
		 */
		innerWidth=(int)(_innerWidth*ratio);
		innerHeight=(int)(_innerHeight*ratio);
		actualHeight=(int)(_actualHeight*ratio);
		/*
		 * Poz�ci�k
		 */
		evenxoffset=xoffset+(double)innerWidth/2.0; //P�ros sorokat beljebb toljuk(0-t�l sz�mozva p�ros)
		yoffset=-(double)(actualHeight-innerWidth)/2.0; //Az elemeket feljebb kell tolni egy "cs�csnyit" a m�hsejt-szerkezet miatt.
		/*
		 * Rajzol�s
		 * 
		 * Hatsz�g cs�csai a komponenes poz�ci�j�hoz viszony�tva.
		 * A hatsz�g egy bal �s egy jobb oldali n�gysz�gb�l �ll, hogy 2 sz�n� is lehessen egy cella
		 * 
		 */
		/*
		 * Bal n�gysz�g
		 *				bal1										fels�										als�										bal2
		 */
		_lRX = new int[]{0											,innerWidth/2								,innerWidth/2								,0};
		_lRY = new int[]{(actualHeight-innerHeight)/2				,0											,actualHeight								,(actualHeight-innerHeight)/2+innerHeight};
		/*
		 * Jobb n�gysz�g
		 * 				fels�										jobb1										jobb2										als�
		 */
		_rRX = new int[]{innerWidth/2								,innerWidth									,innerWidth									,innerWidth/2};
		_rRY = new int[]{0											,(actualHeight-innerHeight)/2				,(actualHeight-innerHeight)/2+innerHeight	,actualHeight};
		/*
		 * Kicsit eltoljuk �ket, hogy legyen r�s a komponensek k�z�tt:
		 */
		_lRX[0]+=xoffsetpercell; //A bal sz�l�t...
		_lRX[3]+=xoffsetpercell;
		_rRX[1]-=xoffsetpercell; //a jobb sz�l�t
		_rRX[2]-=xoffsetpercell;
		_lRY[1]+=yoffsetpercell; //a tetej�t
		_lRY[2]-=yoffsetpercell; //az alj�t
		_rRY[0]+=yoffsetpercell;
		_rRY[3]-=yoffsetpercell; //mind a k�zepe fel� h�zzuk, hogy megjelenjen a s�v
		/*
	     * Azonos�t� felirat be�ll�t�sa
	     */
	    textPos[0]=(innerWidth)/2-3;
	    if ( ratio < minPosRatio ) {  //Ez esetben a nevet rakjuk k�z�pre f�gg�legesen is
	    	textPos[1]=(int)(actualHeight-(innerHeight)/1.5);
	    }
	    else { //Am�gy a n�v ker�lj�n a bels� n�gyzetes r�sz tetej�re
	    	textPos[1]=actualHeight-innerHeight;
	    }
	    fontSize=Math.max(9,(int)((9.0/25.0)*(double)ratio)); //Kb ez a j� ar�ny
	}
	
	public void setElem(Elem e){
		elem=e;
		switch ( e.getType() ) {
			case BOLY:
				color = Color.ORANGE;
				break;
			case FOLD:
				color = Color.LIGHT_GRAY;
				break;
			case HANGYA:
				color = Color.CYAN;
				break;
			case HANGYALESO:
				color = Color.RED;
				break;
			case KAVICS:
				color = Color.DARK_GRAY;
				break;
			case HANGYASZSUN:
				color = Color.MAGENTA;
				break;
			case RAKTAR:
				color = Color.WHITE;
				break;
			case TOCSA:
				color = Color.BLUE;
				break;
		}
		/*
		 * Tulajdons�gok alapj�n sz�nezet.
		 */
		if (e.getProperties().contains(Property.MERGEZETT)) {
			secondColor = Color.PINK;
			
		} else if (e.getProperties().contains(Property.HANGYASZAG)) {
			secondColor = Color.YELLOW;
		} else if (e.getProperties().contains(Property.ELELEMSZAG)) {
			secondColor = Color.GREEN;
		} else
			secondColor = color;
		/*
		 * Ha F�ldr�l van sz�, akkor 2 tulajdons�g-sz�nt is kaphat.
		 */
		if ( e.getType() == Type.FOLD ) {
			/*
			 * Megn�zz�k van-e t�bbf�le tulajdons�ga
			 */
			if (e.getProperties().containsAll(Property.list(Property.MERGEZETT,Property.HANGYASZAG))) { //Ha van, mutassuk a m�rget �s a hangyaszagot
				color=Color.PINK;
				secondColor=Color.YELLOW;
			}
			else if (e.getProperties().containsAll(Property.list(Property.MERGEZETT,Property.ELELEMSZAG))) { //Ha nincs Hangyaszag, mutassuk a m�rget �s az �lelemszagot
				color=Color.PINK;
				secondColor=Color.GREEN;
			}
			else if (e.getProperties().containsAll(Property.list(Property.ELELEMSZAG,Property.HANGYASZAG))) { //Ha csak ez a kett� van, mutassuk �ket
				color=Color.YELLOW;
				secondColor=Color.GREEN;
			}
			/*
			 * Max egy fajta tulajdons�ga van
			 */
			else if ( e.getProperties().contains(Property.MERGEZETT)) {
				secondColor=color=Color.PINK;
			}
			else if ( e.getProperties().contains(Property.HANGYASZAG)) {
				secondColor=color=Color.YELLOW;
			}
			else if ( e.getProperties().contains(Property.ELELEMSZAG)) {
				secondColor=color=Color.GREEN;
			}
			/*
			 * Nincs tulajdons�g, marad a F�ld sz�n
			 */
			else {
				secondColor=color;
			}
		}
		textColor = getComplement(color); //A felirat sz�ne. Komplementer sz�n, hogy mindig j�l l�tsz�djon.
		/*
		 * Tooltip
		 */
		String ptext="";
		if ( e.getProperties().size() > 0) {
			ptext="Tulajdons�gok: ";
			for (Property p : e.getProperties())
				ptext+=p.name()+" ";
			ptext=ptext.trim();
		}
		//Be�p�tett HTML parser van...po�n ez a JAVA.
		setToolTipText(	"<html>\n" +
						"T�pus: "+e.getType().name()+"<br />\n"+
						"Poz�ci�: "+e.getX()+", "+e.getY()+"<br />\n"+
						( !ptext.isEmpty() ? ptext+"<br />\n" : "" )+
						e.extraInfo().trim()+
						"</html>"
					 );
	}
	
	

}
