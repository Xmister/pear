package view;

import shared.ErrorLevel;
import shared.PearEventAdapter;
import model.Palya;

public interface PearView extends Runnable {
	public void setPalya(Palya palya);
	public PearEventAdapter getPearEventAdapter();
	public void setLogLevel(ErrorLevel level);
}
