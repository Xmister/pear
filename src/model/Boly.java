package model;

import shared.PearEventSource;

public final class Boly extends Elem {
	private int intensity;
	private int deliveredAmount;
	
	/*
	 * Konstruktor
	 */
	public Boly() {
		super();
		type = Type.BOLY;
		intensity=40;
		
	}
	
	public Boly (int x, int y) {
		super(x, y);
		type = Type.BOLY;
		intensity=40;
	}
	
	/*
	 * Visszaadja a leszallitott elelmek szamat.
	 */
	public int getDeliveredAmount() {
		return deliveredAmount;
	}
	
	/*
	 * Be�ll�tja a leszallitott elelmek szamat.
	 */
	public void setDeliveredAmount(int d) {
		deliveredAmount=d;
	}
	
	/*
	 * Visszaadja a hangya kibocsajtas intenzitasat
	 */
	public int getItenseity() {
		return intensity;
	}
	
	/*
	 * Beallitja a hangya kibocsajtas intenzitasat.
	 */
	public void setItenseity(int i) {
		intensity = i;
	}
	
	/*
	 * Megnoveli a leszallitott elelmek szamat
	 */
	public void deliverFood(Hangya h) {
		if (h.isCarry()) {
			deliveredAmount++;
			h.setCarry(false);
			PearEventSource.fireFoodDelivered(this);
		}
	}
	
	public void addProperty(Property p) {
		//Nek�nk nincsenek tulajdons�gaink. Mindent eldobunk.
	}
	
	/*
	 * Indit egy hangyat
	 */
	public void startAnt() {
		Hangya h = new Hangya(x,y);
		palya.addElem(h);
	}
	
	public void step() {
		steps++;
		//20-asn�l 10enk�nt, 40-esn�l 5-�nk�nt
		//aj�nlott intensity: 5-15
		if (steps%(200/intensity)==0)
			startAnt();
	}
	
	public void setPalya(Palya p) {
		super.setPalya(p);
		//startAnt();
	}
	
	@Override
	public boolean visitBy(Elem e, boolean really) {
		boolean ret=true;
		
		if (e.getType()==Type.HANGYA) {
			if ( really ) deliverFood((Hangya)e);
		} else ret=false;
		
		
		return ret;
	}
	
	public String toString() {
		String out="addAntHill ";
		out+=x+" "+y+" "+getItenseity()+" "+getDeliveredAmount();
		return out;
	}
	
	public String getShortName() {
		return "B";
	}
	
	public String extraInfo() {
		return "�lelem: "+getDeliveredAmount();
	}
}
