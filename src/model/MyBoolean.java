package model;

public class MyBoolean {
	public static boolean valueOf(String s) {
		try {
			int i= Integer.valueOf(s);
			if (i == 1) return true;
			else if (i == 0) return false;
		} catch (Exception e) {}
		return Boolean.valueOf(s);
	}
}
