package model;

import java.util.List;

import shared.PearEventSource;


public final class Fold extends Elem {
	private Type lastVisitorType=null;
	private Elem lastVisitor=null;
	private int szagOwner=-1;
	
	public void finalize() {
		lastVisitorType=null;
		lastVisitor=null;
		super.finalize();
	}
	
	/*
	 * Konstruktor
	 */
	public Fold() {
		super();
		type=Type.FOLD;
	}
	
	public Fold(Type t) {
		super();
		type=t;
	}
	
	public Fold(int x, int y, Type t) {
		super (x, y);
		type = t;
	}
	
	public Fold(int x, int y) {
		this(x,y,Type.FOLD);
	}
	
	@Override
	public boolean visitBy(Elem e, boolean really) {
		boolean ret=true;
		int x=this.x, y=this.y;
		
		lastVisitorType=e.getType();
		lastVisitor=e;
		if ( really ) {
			if (hasProperty(Property.MERGEZETT)) //M�rgez�s tov�bbad�dik
				e.addProperty(Property.MERGEZETT); 
			
			if (e.type == Type.HANGYA) { //Hangya szagnyomot hagy
				addSzag((Hangya)e);
				steps=0; //Uj szagnal ujraindul
			}
			
			if (type == Type.HANGYALESO) { //Les� hangy�t eszik
				if (e.type == Type.HANGYA)
					e.addProperty(Property.DEAD);
			}
		}
		
		if ( type == Type.TOCSA ) 
			ret = false;
		
		if ( type==Type.KAVICS && ( e.type == Type.HANGYASZSUN || (e.type == Type.KAVICS && ((Fold)e).getLastVisitorType() == Type.HANGYASZSUN) ) ) {
			Hangyaszsun h;
			if ( e.getType() == Type.HANGYASZSUN ) h=(Hangyaszsun) e;
			else h=(Hangyaszsun) ((Fold)e).getLastVisitor();
			int[] res=Dir.getPos(x, y, h.getLastDir());
			x=res[0];
			y=res[1];
			List<Elem> list=palya.validPos(this, x, y);
			ret=(list != null);
			if ( really && ret ) {
				for (int i=0; i<list.size(); i++){
					//if ( !list.get(i).visitBy(this,true) ) PearConsoleHelper.E("Fold false visitBy ellen�rz�s ut�n");
					list.get(i).visitBy(this,true);
				}
				palya.changeMyPos(this, x, y);
			}
			
		}
		else if ( getType() == Type.FOLD && (e.getType().ordinal() > Type.lastFold || e.getType() == Type.KAVICS) ) ret = true;
		else if ( getType() == Type.HANGYALESO && e.getType().ordinal() > Type.lastFold ) ret = true;
		else ret = false;

		return ret;
	}
	
	public String toString() {
		String out="";
		switch (type) {
			case KAVICS:
				out="addStone ";
				break;
			case TOCSA:
				out="addWater ";
				break;
			case HANGYALESO:
				out="addAntLion ";
				break;
			default:
				out="addGround ";
		}
		out+=x+" ";
		out+=y+" ";
		if (type == Type.FOLD) out+=steps+" ";
		if ( palya.isSaveID() && szagOwner != -1 ) out+=szagOwner+" ";
		for ( Property p: properties) {
			out+=p.name()+" ";
		}
		return out.trim();
	}
	
	public int getSteps() {
		return steps;
	}

	public void setSteps(int steps) {
		this.steps = steps;
	}
	

	public int getSzagOwner() {
		return szagOwner;
	}
	

	public void setSzagOwner(int szagOwner) {
		this.szagOwner = szagOwner;
	}
	

	public Type getLastVisitorType() {
		return lastVisitorType;
	}

	public Elem getLastVisitor() {
		return lastVisitor;
	}
	

	/*
	 * (non-Javadoc)
	 * @see Elem#step()
	 */
	public void step() {
		super.step();
		if (hasProperty(Property.HANGYASZAG)) {
			steps++;
			if ( steps > 10 ) {
				properties.remove(Property.HANGYASZAG);
				steps=0;
				PearEventSource.fireCellChanged(getX(), getY());
			}
		}
		if (hasProperty(Property.MERGEZETT)) {
			meregSteps++;
			if ( meregSteps > 12 ) {
				properties.remove(Property.MERGEZETT);
				meregSteps=0;
				PearEventSource.fireCellChanged(getX(), getY());
			}
		}
	}

	private void addSzag(Hangya h) {
		addProperty(Property.HANGYASZAG);
		szagOwner=h.getId();
	}
	
	public boolean isSzagOwner(Hangya h) {
		if ( szagOwner != -1 ) return szagOwner == h.getId();
		return false;
	}
	
	public void addProperty(Property p) {
		switch (p) {
			case SZAGTALAN:
					properties.remove(Property.HANGYASZAG);
					properties.remove(Property.ELELEMSZAG);
				break;
			case NINCSELSZAG:
					properties.remove(Property.ELELEMSZAG);
				break;
			case MERGEZETT:
				if ( type == Type.FOLD ) {
					properties.add(p);
				}
				break;
			case FRISS:
				break;
			default:
				properties.add(p);
		}
		PearEventSource.fireCellChanged(getX(), getY());
	}
	
	public String getShortName() {
		switch (type) {
			case KAVICS:
				return "K";
			case TOCSA:
				return "V";
			case HANGYALESO:
				return "L";
			default:
				return "F";
		}
	}
	
}
