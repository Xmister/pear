package model;

import java.util.List;
import java.util.Random;


public final class Hangya extends Allat {
	private boolean carry;
	private int startx;
	private int starty;
	
	/*
	 * Konstruktor
	 */
	public Hangya(int x, int y) { 
		type = Type.HANGYA; 
		startx=this.x=x;
		starty=this.y=y;
	}
	
	/*
	 * Beallitja a carry erteket
	 */
	public void setCarry(boolean c) {
		if ( carry && !c ) { //Leraktuk a kaj�t
			lastDir=Dir.Random(); //Na szaladjunk
		}
		carry = c;
	}
	
	public boolean isCarry() {
		return carry;
	}
	
	public void setStartPos(int x, int y) {
		startx=x;
		starty=y;
	}
	
	protected List<Elem> move() {
		
		int fordulSzazalek = 15;
		
		List<List<Elem>> list = sense();
		if ( dir == Dir.AUTO ) {
			if (isCarry()) { //Vissza a bolyba
				lastDir=getDir(startx, starty, list);
				fordulSzazalek = 5;
			} else {
				int[] raktar = null;
				int[] elelemszag = null;
				int[] hangyaszag = null;
				
				//n�zz�k meg l�tunk-e rakt�rat, �rz�nk-e �lelem vagy esetleg hangyaszagot
				if (list!=null) {
					for (List<Elem> l : list) {
						if (l!=null) {
							for (Elem e : l) {
								if (e.type == Type.RAKTAR && ((Raktar)e).getLeftAmmount()>0) { //csak ha nem �res rakt�r, �s n�lunk nincs m�r kaja
									//ha nem tal�ltunk m�g c�lt, vagy a tal�lt c�l manhattan-t�vols�ga nagyobbb
									if (raktar==null || (Math.abs(x-raktar[0])+Math.abs(getY()-raktar[1])) > Math.abs(x-e.getX())+Math.abs(getY()-e.getY()))
										raktar = new int[] {e.getX(), e.getY()};
								}
								if (e.hasProperty(Property.ELELEMSZAG)) {
									if (elelemszag==null || (Math.abs(x-elelemszag[0])+Math.abs(getY()-elelemszag[1])) > Math.abs(x-e.getX())+Math.abs(getY()-e.getY()))
										elelemszag = new int[] {e.getX(), e.getY()};
								}
								if ( e.hasProperty(Property.HANGYASZAG) && !e.isSzagOwner(this) ) { //Csak akkor k�vess�k a hangyaszagot, ha nem a mi�nk.
									if (hangyaszag==null || (Math.abs(x-hangyaszag[0])+Math.abs(getY()-hangyaszag[1])) > Math.abs(x-e.getX())+Math.abs(getY()-e.getY()))
										hangyaszag = new int[] {e.getX(), e.getY()};
								}
							}
						}
					}
				}
				
				if (raktar != null) { //els� priorit�s� a rakt�r
					lastDir=getDir(raktar[0], raktar[1], list);
					fordulSzazalek = 5;
				} else if (elelemszag!=null) { //m�sodik priorit�s� az �lelemszag (mivel rakt�rn�l van)
					lastDir=getDir(elelemszag[0], elelemszag[1], list);
					fordulSzazalek = 7;
				} else if (hangyaszag!=null) { //harmadik a hangyaszag
					Random r = new Random(System.currentTimeMillis());
					if (r.nextInt(100) < 15) { //Csak 15%-ban k�vess�k
						lastDir=getDir(hangyaszag[0], hangyaszag[1], list);
						fordulSzazalek = 75;
					}
				}
			}// Innent�l ha visz, ha nem
			
			if ( palya.isRandom() ) {
				//N�ha forduljunk el, csak mert hangy�k vagyunk
				Random r = new Random(System.currentTimeMillis());
				int rnd = r.nextInt(100);
				//Random elfordul�s
				if (rnd < fordulSzazalek)
					lastDir = Dir.getNeighbour(rnd%2==0, lastDir);
			}
			
			if ( lastDir == Dir.NONE ) { //Celnal vagyunk mar egy lepes ota
				lastDir = Dir.Random(); //Elakadtunk...nem kellene, de megt�rt�nt. H�t menj�nk valamerre.
			}
		} else lastDir=dir;
		
		List<Elem> ret=super.move();
		
		return ret;
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see Allat#step()
	 */
	@Override
	public void step() {
		
		move();
		if (hasProperty(Property.MERGEZETT)) {
			steps++;
			if ( steps > 4 ) {
				properties.remove(Property.MERGEZETT);
				properties.add(Property.DEAD);
				steps=0;
			}
		}
	}
	
	public String toString() {
		String out="addAnt ";
		out+=x+" "+getY()+" "+carry+" ";
		for (Property p:properties) {
			out+=p.name()+" ";
		}
		return out.trim();
	}
	
	public String getShortName() {
		return "H";
	}
	
	public boolean visitBy(Elem e, boolean really) {
		boolean ret = super.visitBy(e, really);
		if (ret) {
			if ( e.getType() != Type.HANGYASZSUN ) ret=false;
		}
		return ret;
	}
	
	public String extraInfo() {
		return "Hordoz: "+isCarry();
	}
	
}
