package model;


import java.util.*;

public abstract class Elem {
	protected int x;
	protected int y;
	protected int steps;
	protected int meregSteps;
	protected Type type;
	protected Set<Property> properties = new HashSet<Property>();
	protected Palya palya;
	protected int id = System.identityHashCode(this);
	
	public Elem() {
		
		steps=0;
		
	}
	
	public Elem(int x, int y) {
		this();
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public void setPalya(Palya p) {
		palya = p;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	//hoppa, ilyenunk nincs a diagramon
	public Set<Property> getProperties() {
		return properties;
	}
	
	public boolean hasProperty(Property p) {
		return properties.contains(p);
	}
	
	public boolean isSzagOwner(Hangya h) { return false; }
	
	public Type getType() {
		return type;
	}
	
	public void addProperty(Property p) {
		properties.add(p);	
	}
	
	/*
	 * Az ido egy lepesere meghivodo metodus.
	 */
	public void step() {
		
	}
	
	public boolean visitBy(Elem e, boolean really) {
		
		
		return true;
	}
	
	public String getShortName() {
		return "EL";
	}
	
	public void finalize() {
		palya=null;
		try {
			super.finalize();
		} catch (Throwable e) {}
	}
	
	public String extraInfo() {
		return "";
	}
}
