package model;

public final class Eszkoz {
	private int count;
	private int radius;
	private Palya palya;
	private Property property;
	
	/*
	 * Konstruktor.
	 * @param p: Palya, amit ez az objektum is eltarol.
	 * @param c: count valtozo kezdeti erteke.
	 * @param r: radius valtozo kezdeti erteke.
	 */
	public Eszkoz(Palya p, int c, int r) {
		
		palya = p;
		count = c;
		radius = r;
		
	}
	
	/*
	 * Ezzel a fugvennyel lehet kezdemenyezni az eszkoz hasznalatat.
	 */
	public void use (int x, int y) {
		
		if ( count > 0 ) {
			palya.setEffect(x, y, radius, property);
			count--;
		}
		
	}
	
	/*
	 * Visszaadja a count valtozo erteket.
	 */
	public int getCount() {
		return count;
	}
	
	/*
	 * Beallitja a count valtozo erteket.
	 */
	public void setCount(int c) {
		count = c;
	}
	
	/*
	 * Visszaadja a radius valtozo erteket.
	 */
	public int getRadius() {
		return radius;
	}
	
	/*
	 * Beallitja a radius valtozo erteket.
	 */
	public void setRadius(int r) {
		radius = r;
	}
	
	/*
	 * Visszaadja a property valtozo erteket.
	 */
	public Property getProperty() {
		return property;
	}
	
	/*
	 * Beallitja a property valtozo erteket.
	 */
	public void setProperty(Property p) {
		property = p;
	}
	
	/*
	 * Beallitja a palya referenciat
	 */
	public void setPalya(Palya p) {
		palya=p;
	}
}
