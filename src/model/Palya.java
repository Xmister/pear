package model;

import java.util.*;

import shared.PearEvent;
import shared.PearEventAdapter;
import shared.PearException;
import shared.PearEventListener;
import shared.PearEventSource;



public final class Palya {
	private PearEventAdapter pe = new PearEventAdapter() {
		@Override
		public void onFoodDelivered(PearEvent event, Boly boly) {
			if ( boly.getDeliveredAmount() == foodCount ) {
				timer.cancel();
				timer=null;
				PearEventSource.fireMapStep(allSteps); //Hogy a View-ok tudj�k hanyadik l�p�sn�l vagyunk
				PearEventSource.fireGameOver(allSteps); //Min�l t�bb l�p�s, ann�l �gyesebb volt valaki.
			}
		}
	};
	
	private int width;
	private int height;
	private int bolyCount=0;
	private int sunCount=0;
	private int foodCount=0; //Ebben t�roljuk a p�ly�n l�v� �lelem mennyis�g�t, �gy ellen�r�zni tudjuk, amikor mindet begy�jt�tt�k a hangy�k.
	private int sunIntensity=50, sunSteps=0; //Milyen s�r�n jelenjen meg a s�n. Fel�l�rjuk k�s�bb. sunSteps: Ennek a sz�mol�s�hoz
	private boolean random=true;
	private int steps=-1, allSteps=0; //steps: Meg ennyit lepunk. Ha <0, akkor vegtelen. allSteps: �sszesen ennyit l�pt�nk ezen a p�ly�n.
	private Boly boly=null;
	private boolean saveID=true;
	private Timer timer;
	private long timer_step=700; //0.7 sec
	
	private List<Elem>[][] elemek;
	
	public void finalize() {
			PearEventSource.removeEventListener(pe);
			pe=null;
		if ( timer != null ) {
			timer.cancel();
			timer=null;
		}
		for ( List<Elem>[] row : elemek)
			if ( row != null ) for (List<Elem> cell : row)
				if ( cell != null ) for (Elem e: cell)
					e.finalize();
		boly=null;
		elemek=null;
	}
	
	/*
	 * Konstruktor.
	 * @param w: Palya szelessege
	 * @param h: Palya magassaga
	 */
	@SuppressWarnings("unchecked")
	public Palya(int w, int h, boolean n) {
		PearEventSource.addEventListener(pe); //�nmagunkat is hozz�adjuk
		
		width = w;
		height = h;
		/*
		 * Palya elemeinek inicializalasa
		 */
		elemek = new List[width][height];
		timer = new Timer("playa_timer");
		if ( n ) {
			//Alap Fold-ek legeneralasa
			for ( int i=0; i<elemek.length; ++i ) {
				List<Elem>[] row = elemek[i];
				for ( int j=0; j<row.length; ++j ) {
					List<Elem>cell = new ArrayList<Elem>();
					Fold f = new Fold(i,j);
					f.setPalya(this);
					cell.add(f);
					row[j]=cell;
				}
			}
			//generate?
		}
		else {
			//�res cell�k legeneralasa
			for ( int i=0; i<elemek.length; ++i ) {
				List<Elem>[] row = elemek[i];
				for ( int j=0; j<row.length; ++j ) {
					List<Elem>cell = new ArrayList<Elem>();
					row[j]=cell;
				}
			}
		}
	}
	
	/*
	 * Gener�l elemeket a p�lya cell�iba
	 */
	public void generate(int per_raktar, int per_tocsa, int per_ko, int per_leso) {
		int nraktar, ntocsa, nko, nleso;
		Random r = new Random(System.currentTimeMillis());
		
		//darabos�t�s
		int alapTerulet = width*height;
		nraktar = per_raktar * alapTerulet / 100;
		if (nraktar < 1) //min egy rakt�r
			nraktar = 1;
		ntocsa = per_tocsa * alapTerulet / 100;
		if (ntocsa < 1)
			ntocsa = 1;
		nko = per_ko * alapTerulet / 100;
		if (nko < 1)
			nko = 1;
		nleso = per_leso * alapTerulet / 100;
		if (nleso < 1)
			nleso = 1;
		
		List<Elem> foglaltPoziciok = new ArrayList<Elem>(1+nraktar+ntocsa+nko);
		
		Boly boly = new Boly(r.nextInt(width-1), r.nextInt(height-1));
		foglaltPoziciok.add(boly);
		addElem(boly);
		
		int x, y;
		boolean foglalt = false;
		
		//T�cs�k
		for (int i=0; i<ntocsa; i++) {
			//Garant�ljuk az elszigetelts�get
			do {
				foglalt = false;
				
				x = r.nextInt(width-1);
				y = r.nextInt(height-1);
				
				for (Elem e : foglaltPoziciok)
					if (e.getX()==x && e.getY()==y)
						foglalt = true;
				
			} while (foglalt);
			Fold f = new Fold(x, y, Type.TOCSA);
			foglaltPoziciok.add(f);
			addElem(f);
		}
		//K�vek
		for (int i=0; i<nko; i++) {
			//Garant�ljuk az elszigetelts�get
			do {
				foglalt = false;
				x = r.nextInt(width-1);
				y = r.nextInt(height-1);
	
				for (Elem e : foglaltPoziciok)
					if (e.getX()==x && e.getY() ==y)
						foglalt = true;
			} while (foglalt);
			Fold f = new Fold(x, y, Type.KAVICS);
			foglaltPoziciok.add(f);
			addElem(f);
		}
		//Rakt�rak
		for (int i=0; i<nraktar; i++) {
			//Garant�ljuk az elszigetelts�get
			do {
				foglalt = false;
				x = r.nextInt(width-1);
				y = r.nextInt(height-1);
				
				for (Elem e : foglaltPoziciok)
					if (e.getX()==x && e.getY() ==y)
						foglalt = true;
				
				//Minimum l�gvonalbeli t�vols�g a boly �s a rakt�r k�z�tt
				if (Math.sqrt((x-boly.x)*(x-boly.x) + (y-boly.y)*(y-boly.y)) < width/12)
					foglalt = true;
			} while (foglalt);
			
			Raktar raktar = new Raktar(x, y);
			foglaltPoziciok.add(raktar);
			addElem(raktar);
		}
		//Hangyales�k
		for (int i=0; i<nleso; i++) {
			//Garant�ljuk az elszigetelts�get
			do {
				foglalt = false;
				x = r.nextInt(width-1);
				y = r.nextInt(height-1);
				for (Elem e : foglaltPoziciok)
					if (e.getX()==x && e.getY() ==y)
						foglalt = true;
				
				//Minimum l�gvonalbeli t�vols�g a boly �s a les� k�z�tt
				if (Math.sqrt((x-boly.x)*(x-boly.x) + (y-boly.y)*(y-boly.y)) < width/20)
					foglalt = true;
			} while (foglalt);
			
			Fold leso = new Fold(x, y, Type.HANGYALESO);
			foglaltPoziciok.add(leso);
			addElem(leso);
		}
		//Kezd� hangya
		Hangya h = new Hangya(boly.getX(), boly.getY());
		addElem(h);
	}
	
	/*
	 * Visszaadja a palya szelesseget
	 */
	public int getWidth() {
		return width;
	}	
	
	public boolean isSaveID() {
		return saveID;
	}

	public void setSaveID(boolean saveID) {
		this.saveID = saveID;
	}

	public boolean isRandom() {
		return random;
	}

	public boolean setRandom(boolean random) {
		this.random=random;
		PearEventSource.fireSetRandomFinished(random);
		return true;
	}	

	public int getSteps() {
		return steps;
	}

	public boolean setSteps(int steps) {
		this.steps=steps;
		if ( timer != null ) {
			timer.cancel();
			timer = null;
		}
		if ( steps != 0 ) {
			clearProperties(Property.list(Property.FRISS)); //Az els� l�p�sn�l indulhatnak r�gt�n.
			timer = new Timer();
			timer.schedule(new TimerTask() {
				  @Override
				  public void run() {
				    step();
				  }
				}, timer_step); //0.7 sec
		}
		return true;
	}
	
	public List<Elem> validPos(Elem e,int x, int y) {
		if ( x >= width || y >= height || x < 0 || y < 0)
			return null;
		List<Elem> ret = elemek[x][y];
		if ( e != null) {
			for (int j=0; ret!=null&&j<ret.size(); j++){
				if (!ret.get(j).visitBy(e,false)) { //Tudunk oda l�pni?
					ret=null;
					break;
				}
			}
		}
		return ret;
	}
	
	private Elem contains(List<Elem> cell, List<Type> type) {
		if (cell != null) {
			for (Elem e: cell) {
				if ( type.contains(e.getType()) ) {
					return e;
				}
			}
		}
		return null;
	}
	
	private void insertElem(Elem e) {
		e.addProperty(Property.FRISS);
		e.setPalya(this);
		elemek[e.getX()][e.getY()].add(e);
		PearEventSource.fireElemAdded(e);
		PearEventSource.fireCellChanged(e.getX(), e.getY());
		switch (e.getType()) {
			case HANGYASZSUN:
				sunCount++;
				break;
			case BOLY:
				boly=(Boly)e;
				bolyCount++;
				break;
			case RAKTAR:
				foodCount+=((Raktar)e).getLeftAmmount();
				PearEventSource.fireFoodCountChanged(foodCount);
				sunIntensity=(foodCount/10); //F�ggj�n az �tel sz�m�t�l
				sunIntensity = Math.max(Math.min(sunIntensity, 100), 50); //De legyen 50 �s 100 k�z�tt.
				break;
		}
	}

	public boolean addElem(Elem e) {
		if ( e.getType() == Type.BOLY) {
			System.out.println();
		}
		if ( e.getType() == Type.BOLY && bolyCount > 0 ) return false;
		if ( e.getType() == Type.HANGYASZSUN && sunCount > 0 ) return false;
		if ( validPos(null,e.getX(),e.getY()) == null ) return false;
		if ( elemek[e.getX()][e.getY()] == null) elemek[e.getX()][e.getY()] = new ArrayList<Elem>();
		if ( elemek[e.getX()][e.getY()].size() == 0) {
			insertElem(e);
			return true;
		}
		else {
			boolean ok=true;
			for (Elem i : elemek[e.getX()][e.getY()]) {
				if ( (i.getType() != Type.FOLD) 
						&&
						!( 
								(e.getType() == Type.HANGYA && i.getType() == Type.BOLY )
								|| //Hangy�t �s BOLYt ak�r rakhatunk is egym�sra
								(e.getType() == Type.BOLY && i.getType() == Type.HANGYA )
						)
					)	
				{
					ok=false;
					break;
				}
			}
			if (ok) {
				if ( e.getType().ordinal() <= Type.lastFold ) //Fold t�pusai.
					elemek[e.getX()][e.getY()].clear(); //Ha valamilyen Fold-et adunk hozza, az addigi Fold-et t�r�lj�k.
				insertElem(e);
			}
			return ok;
		}
	}
	
	public Elem getElemByID(int id) {
		if (id == -1) return null;
		for (List<Elem>[] row: elemek)
			for(List<Elem> cell: row)
				if ( cell != null ) for (Elem e: cell) {
					if (e.getId() == id) return e;
				}
		return null;
	}
	
	/*
	 * Visszaadja a palya magassagat
	 */
	public int getHeight() {
		
		
		return height;
	}
	
	/*
	 * Adott teruleten beallit egy tulajdonsagot.
	 * @param x: X koordinata
	 * @param y: Y koordinata
	 * @param r: A kor sugara
	 * @param p: A beallitando tulajdonsag
	 */
	public void setEffect (int x, int y, int r, Property p) {
		
		
		for (List<Elem> le : getElems(x,y,r))
			if (le != null)
				for (Elem e : le) {
					e.addProperty(p);
					PearEventSource.fireCellChanged(e.getX(), e.getY());
				}
		
		
	}
	
	/*
	 * Megvaltoztatja az elem poziciojat
	 * @param e: Az elem, ami poziciot valt
	 * @param x: X koordinata
	 * @param y: Y koordinata
	 */
	public List<Elem> changeMyPos(Elem e, int x, int y) { //�j koordin�t�kat adjuk �t, r�giek lek�rhet�k
		
		List<Elem> list = null;
		if ( e.getType() == Type.HANGYASZSUN && contains(elemek[x][y], Type.list(Type.RAKTAR,Type.BOLY,Type.TOCSA)) == null ) { //Nincs ott TO/RAKTAR/BOLY.
			list=validPos(e,x,y);
		}
		else if ( e.getType() == Type.HANGYA && contains(elemek[x][y], Type.list(Type.KAVICS,Type.TOCSA)) == null ) { //Nincs ott se KO, se TO.
			list=validPos(e,x,y);
		}
		else if ( contains(elemek[x][y], Type.list(Type.TOCSA,Type.HANGYA,Type.HANGYASZSUN,Type.HANGYALESO)) == null ) { //KO
			list=validPos(e,x,y);
		}
		if (list != null) {
			if ( e.getType() == Type.KAVICS ) {
				Fold regi=(Fold)elemek[e.getX()][e.getY()].get(0);
				Fold uj=(Fold)elemek[x][y].get(0);
				regi.type=Type.FOLD;
				uj.type=Type.KAVICS;
			}
			else {
				e.addProperty(Property.LEPETT);
				elemek[e.getX()][e.getY()].remove(e);
				elemek[x][y].add(e);
			}
			PearEventSource.fireElemMoved(e,x,y);
			PearEventSource.fireCellChanged(e.getX(),e.getY());
			PearEventSource.fireCellChanged(x,y);
		}
		
		return list;
	}
	
	/*
	 * Rekurziv fv a sugar altal erintett mezok megkeresesere
	 * @param x: X koordinata
	 * @param y: Y koordinata
	 * @param r: A kor sugara
	 * @param selected: Erintett mezok
	 * @param processed: Mezok, amik kornyezete mar fel van deritve
	 */
	private void getElems(int x, int y, int r, Set<List<Elem>> selected, Set<List<Elem>> processed) {
				if (r==0) return;
				
				if ( validPos(null,x,y) == null) return; //A sugar tulmutat a palyan
				
				if (processed.contains(elemek[x][y])) return; //Ot mar feldolgoztuk
				
				processed.add(elemek[x][y]); //Most dolgozzuk fel
				
				selected.add(elemek[x][y]); //Hozzadjuk a list�hoz
				//szomsz�dok, �s azok szomszedai a sugarunkon belul
				if (r > 1) {
					getElems(x-1, y, r-1,selected,processed); //balra
					getElems(x+1, y, r-1,selected,processed); //jobbra
					
					int nx = x;
					if (y%2!=0 ) nx= x-1;

					getElems(nx, y-1, r-1,selected,processed); //balra-fel
					getElems(nx+1, y-1, r-1,selected,processed); //jobbra-fel
					getElems(nx, y+1, r-1,selected,processed); //balra-le
					getElems(nx+1, y+1, r-1,selected,processed); //jobbra-le
				}
				
	}
	
	public List<List<Elem>> getElems() {
		List<List<Elem>> list = new ArrayList<List<Elem>>(getWidth()*getHeight());
		for (List<Elem>[] row : elemek)
			for ( List<Elem> cell : row)
				if ( cell != null ) list.add(cell);
		return list;
	}
	
	public List<Elem>[][] getElemsArray() {
		return elemek;
	}
	
	/*
	 * Visszaadja adott teruleten levo cellak(es a rajtuk levo elemek) listajat
	 * @param x: X koordinata
	 * @param y: Y koordinata
	 * @param r: A kor sugara
	 */
	public List<List<Elem>> getElems(int x, int y, int r) {
		
		Set<List<Elem>> selected = new HashSet<List<Elem>>();
		Set<List<Elem>> processed = new HashSet<List<Elem>>();
		getElems(x,y,r,selected,processed);
		
		
		return new ArrayList<List<Elem>>(selected);
	}
	
	private void killItWithFire(Elem e) {
		switch (e.getType()) {
			case HANGYASZSUN:
				sunCount--;
				sunSteps=0;
				break;
			case HANGYA:
				if (((Hangya) e).isCarry()) {
					decFoodCount();
				}
				break;
		}
		PearEventSource.fireElemKilled(e);
		elemek[e.getX()][e.getY()].remove(e);
		PearEventSource.fireCellChanged(e.getX(), e.getY());
		e.finalize();
	}
	
	private void decFoodCount() {
		foodCount--; //Egy kaja elveszett
		PearEventSource.fireFoodCountChanged(foodCount);
		if ( boly.getDeliveredAmount() == foodCount ) {
			timer.cancel();
			timer=null;
			PearEventSource.fireMapStep(allSteps); //Hogy a View-ok tudj�k hanyadik l�p�sn�l vagyunk
			PearEventSource.fireGameOver(allSteps); //Min�l t�bb l�p�s, ann�l �gyesebb volt valaki.
		}
	}
	
	public void addEventListener(PearEventListener pe) {
		PearEventSource.addEventListener(pe);
	}
	
	public void clearProperties(List<Property> ps) {
		for (List<Elem>[] row : elemek)
			for (List<Elem> cell : row)
				if (cell != null)
					for (Elem e : cell)  {
						for (Property p : ps) {
							e.getProperties().remove(p);
						}
					}
	}
	
	/*
	 * A jatek (es az elemek) egyet lepnek
	 */
	public void step() {
		if ( boly.getDeliveredAmount() == foodCount ) return; //Ha m�r v�ge, nem csin�lunk semmit.
		//HashMap<Integer,HashMap<Integer,Elem>> toRemove = new HashMap<Integer,HashMap<Integer,Elem>>();
		if ( steps != 0) {
			PearEventSource.fireMapStep(allSteps+1);
			if ( sunCount == 0 && (sunSteps+1) % sunIntensity  == 0 ) {
				int x, y;
				Hangyaszsun s;
				do {
					Random r=new Random(System.currentTimeMillis());
					if ( r.nextBoolean() ) { //A p�lya jobb-bal sz�l�n l�p be
						if ( r.nextBoolean() ) x=getWidth()-1;
						else x=0;
						y=r.nextInt(getHeight());
					}
					else { //A p�lya als�-fels� sz�l�n l�p be
						if ( r.nextBoolean() ) y=getHeight()-1;
						else y=0;
						x=r.nextInt(getWidth());
					}
					s = new Hangyaszsun(x, y);
				} while ( !addElem(s) );
				PearEventSource.fireAddAntEaterFinished(s); //A model jobb ha nem ismeri a Controller-t, meg a Command-ot, ez�rt ezt a s�n hozz�ad�st saj�t magunk kezelj�k.
			}
			//int rowIndex, cellIndex;
			for(int i = 0; i < elemek.length; i++) {
				List<Elem>[] row = elemek[i];
				for (int j = 0; j < row.length; j++) {
					List<Elem> cell = row[j];
					for (int k = 0; k < cell.size(); k++) {
						Elem e = cell.get(k);
						if (!e.getProperties().contains(Property.LEPETT)) {
							if ( e.getProperties().contains(Property.DEAD))
								killItWithFire(e);
							else if ( e.getProperties().contains(Property.FRISS)) { //Akik most ker�ltek a p�ly�ra, azokat ne l�ptess�k r�gt�n.
								e.getProperties().remove(Property.FRISS);
							}
							else 
								e.step();
						}
					}
				}
			}
			
			clearProperties(Property.list(Property.LEPETT,Property.FRISS));
			
			steps--;
			allSteps++;
			sunSteps++;
			if ( timer != null)
				timer.schedule(new TimerTask() {
					  @Override
					  public void run() {
					    step();
					  }
					}, timer_step);
		}
		
	}
	
	/*
	 * Menteshez visszaadja a palya informacioit egy String-ben.
	 */
	public String getAll(boolean cont) {
		
		String out="";
		for (List<Elem>[] row: elemek)
			for(List<Elem> cell: row)
				if ( cell != null ) for (Elem e: cell) {
					if ( e.getType().ordinal() < Type.lastFold ) {//A Rakt�rat csak akkor adhatjuk hozz�, ha minden m�s F�ld-szer� megvan, hogy be tudja �ll�tani az elemszagot
						out+=e.toString();
						if (saveID) out+=" "+e.getId();
						out+="\n";
					}
				}
		for (List<Elem>[] row: elemek)
			for(List<Elem> cell: row)
				if ( cell != null ) for (Elem e: cell) {
					if ( e.getType().ordinal() >= Type.lastFold ) {//Ut�na j�het a t�bbi
						out+=e.toString();
						if (saveID) out+=" "+e.getId();
						out+="\n";
					}
				}
		out+="allStep "+allSteps+"\n";
		out+="step "+(cont ? "0" : "-1");
		return out;
	}
	
	public boolean setAllSteps(int s) {
		allSteps=s;
		return true;
	}
	
	public boolean addProperty(int x, int y, Property p) throws PearException {
		String err="Tualjdonsag beallitasa nem sikerult.";
		try {
			for (Elem e: elemek[x][y]) {
				e.addProperty(p);
				if ( e.getProperties().contains(p) )
					PearEventSource.fireAddPropertyFinished(e, p);
			}
			return true;
		} catch (Exception e) {
			throw new PearException(err+"\n"+e.getMessage());
		}
	}
	
	public boolean setDir(int x, int y, Dir dir) throws PearException {
		boolean van=false;
		String err ="Preferalt irany beallitasa nem sikerult.";
		try {
			for(Elem e: elemek[x][y]) {
				if (e.getType().ordinal() >= Type.HANGYA.ordinal()) { //�llatr�l besz�l�nk.
					van=true;
					Allat a = (Allat) e;
					a.setDir(dir);
				}
			}
			if (!van) throw new PearException("Nem letezik mozgo egyed a megadott koordinatan.");
			else {
				PearEventSource.fireSetDirFinished();
				return true;
			}
		} catch (Exception e) {
			throw new PearException(err+"\n"+e.getMessage());
		}
	}
	
	public boolean showCell(int x, int y) throws PearException {
		String err ="Nincs ilyen cella.";
		String res = "";
		try {
			res+=x+", "+y+" koordin�ta elemei:";
			for(Elem e: elemek[x][y]) {
				String out ="\t"+e.getType().toString()+" ";
				for ( Property p : e.getProperties() ) {
					out+=p.toString()+" ";
				}
				res+=out.trim()+"\n";
			}
			PearEventSource.fireShowCell(res);
			return true;
		} catch (Exception e) {
			throw new PearException(err+"\n"+e.getMessage());
		}
	}

	public Boly getBoly() {
		return boly;
	}
	
	public void faster() {
		if ( timer_step > 1 ) {
			timer_step/=2;
			PearEventSource.fireSpeedChange(timer_step);
		}
	}
	
	public void slower() {
		if ( timer_step < 10000 ) {
			timer_step*=2;
			PearEventSource.fireSpeedChange(timer_step);
		}
	}
	
}
