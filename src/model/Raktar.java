package model;

import shared.PearEventSource;

public final class Raktar extends Elem {
	private int capacity=30;
	private int leftAmount=capacity;
	
	/*
	 * Konstruktor
	 */
	public Raktar() {
		type = Type.RAKTAR;
	}
	public Raktar(int x, int y) {
		super(x, y);
		type = Type.RAKTAR;
	}
	
	public void setPalya(Palya p) {
		super.setPalya(p);
		palya.setEffect(x, y, 2, Property.ELELEMSZAG);
	}
	
	/*
	 * Kivesz egy elelmet, csokkenti a leftAmount valtozot.
	 */
	public boolean takeFood() {
		
		boolean ret=false;
		if (leftAmount > 0 ) {
			leftAmount--;
			ret=true;
		}
		//Csak akkor szagtalaintsunk, ha pont most fogyott el
		if (ret && leftAmount < 1)
			palya.setEffect(x, y, 2, Property.NINCSELSZAG);
		if (ret) PearEventSource.fireStorageChanged(this);
		
		return ret;
	}
	
	public int getLeftAmmount() {
		return leftAmount;
	}
	
	public void setCapacity(int c) {
		capacity=c;
		leftAmount=Math.min(leftAmount, capacity);
	}
	
	public int getCapacity() {
		return capacity;
	}
	public void setLeftAmount(int l) {
		leftAmount=l;
		capacity=Math.max(leftAmount, capacity);
	}
	
	public void addProperty(Property p) {
		//Nekünk nincsenek tulajdonságaink. Mindent eldobunk.
	}
	
	@Override
	public boolean visitBy(Elem e, boolean really) {
		boolean ret=true;
		
		
		if (e.getType()==Type.HANGYA) {
			Hangya h = (Hangya)e;
			if (really && !h.isCarry() && takeFood() ) {
				h.setCarry(true);
			}
		} else ret = false;
		
		
		
		return ret;
	}
	
	public String toString() {
		String out="addFoodStorage ";
		out+=x+" "+y+" "+capacity+" "+leftAmount;
		return out;
	}
	
	public String getShortName() {
		return "R";
	}
	
	public String extraInfo() {
		return "Élelem: "+getCapacity()+"/"+getLeftAmmount();
	}
}
