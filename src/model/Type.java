package model;

import java.util.Arrays;
import java.util.List;

/*
 * Az elemek tipusai.
 * -------------------
 * Ha egy objektumnak tobb tipusa is lehet,
 * azok egymas utan lesznek,
 * igy megkonnyitve a kesobbi ellenorzest.
 * 
 * 'KO' jelzi az utols� Fold t�pust!
 * 'HANGYA' jelzi az elso Allat t�pust!
 */
public enum Type {
	FOLD,TOCSA,HANGYALESO,KAVICS,BOLY,RAKTAR,HANGYA,HANGYASZSUN;
	
	public static int lastFold=RAKTAR.ordinal();
	
	public static List<Type> list(Type... args) {
		return Arrays.asList(args);
	}
}