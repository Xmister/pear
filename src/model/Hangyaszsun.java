package model;

import java.util.*;

import shared.PearEventSource;


public final class Hangyaszsun extends Allat {
	private int bellySize;
	private int antCount;
	//private int steps;
	
	/*
	 * Konstruktor
	 */
	public Hangyaszsun() {
		super();
		type = Type.HANGYASZSUN;
		bellySize=10;
		antCount=0;
		steps=0;
	}
	
	public Hangyaszsun(int x, int y) {
		this();
		setX(x);
		setY(y);
	}
	
	public void addProperty(Property p) {
		if ( Property.list(Property.DEAD, Property.LEPETT).contains(p) )
			super.addProperty(p);
	}
	
	private void eat(Elem e) {
		if (e.getType() == Type.HANGYA && antCount < bellySize) {
			antCount++;
			e.addProperty(Property.DEAD);
			PearEventSource.fireSunAte(this);
		}
	}
	
	public int getBellySize() {
		return bellySize;
	}

	public void setBellySize(int bellySize) {
		this.bellySize = bellySize;
		antCount = Math.min(antCount, bellySize);
	}

	public int getAntCount() {
		return antCount;
	}

	public void setAntCount(int antCount) {
		this.antCount = antCount;
		bellySize = Math.max(bellySize, antCount);
	}
	
	/*
	 * Az atadott elemek kozul megeszi amit szeret.
	 */
	public void eat(List<Elem> list) {
		
		if (list != null)
			for (Elem e:list)
				eat(e);
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see Allat#step()
	 */
	@Override
	public void step() {
		///steps++;
		move();
		//if ( move() != null && steps%3 == 0  ) move(); //Gyorsabbnak kell lennunk a Hangyanal, ezert 3 idokozonkent 2-t lepunk
	}
	
	protected List<Elem> move() {
		
		List<List<Elem>> list = sense();
		int fordulSzazalek = 15;
		
		if ( dir == Dir.AUTO ) {
			
			int[] hangya = null;
			int[] hangyaszag = null;
			
			if ( getBellySize() == getAntCount() ) { //J�l laktunk. Hagyjuk el a p�ly�t amint lehet.
				if ( x == 0 || x == palya.getWidth()-1 || y == 0 || y == palya.getHeight()-1 ) { //M�r a p�lya sz�l�n vagyunk
					addProperty(Property.DEAD);
				} else {
					int x,y;
					if ( palya.getWidth()-1-getX() < getX() ) x = palya.getWidth()-1;
					else x=0;
					if ( palya.getHeight()-1-getY() < getY() ) y = palya.getHeight()-1;
					else y=0;
					lastDir=getDir(x, y, list);
				}
			}
			else {
				//n�zz�k meg l�tunk-e hangy�t vagy esetleg �rz�nk hangyaszagot
				if (list!=null) {
					for (List<Elem> l : list) {
						if (l!=null) {
							for (Elem e : l) {
								if (e.type == Type.HANGYA) {
									//ha nem tal�ltunk m�g c�lt, vagy a tal�lt c�l manhattan-t�vols�ga nagyobbb
									if (hangya==null || (Math.abs(x-hangya[0])+Math.abs(y-hangya[1])) > Math.abs(x-e.getX())+Math.abs(y-e.getY()))
										hangya = new int[] {e.getX(), e.getY()};
								}
								if (e.properties.contains(Property.HANGYASZAG)) {
									if (hangyaszag==null || (Math.abs(x-hangyaszag[0])+Math.abs(y-hangyaszag[1])) > Math.abs(x-e.getX())+Math.abs(y-e.getY()))
										hangyaszag = new int[] {e.getX(), e.getY()};
								}
							}
						}
					}
				}
				
				if (hangya != null) { //els� priorit�s� a hangya
					lastDir=getDir(hangya[0], hangya[1], list);
					fordulSzazalek = 5;
				} else if (hangyaszag!=null) { //m�sodik a hangyaszag
					lastDir=getDir(hangyaszag[0], hangyaszag[1], list);
					fordulSzazalek = 7;
				}
				
				if ( palya.isRandom() ) {
					//N�ha forduljunk el, csak mert.
					Random r = new Random(System.currentTimeMillis());
					int rnd = r.nextInt(100);
					//Random elfordul�s
					if (rnd < fordulSzazalek)
						lastDir = Dir.getNeighbour(rnd%2==0, lastDir);
				}
			}
			
			if ( lastDir == Dir.NONE ) { //Celnal vagyunk mar egy lepes ota
				lastDir = Dir.Random(); //Elakadtunk...nem kellene, de megt�rt�nt. H�t menj�nk valamerre.
			}
		} else lastDir=dir;
		
		List<Elem> ret=super.move();
		eat(ret);
		
		return ret;
	}
	
	@Override
	public boolean visitBy(Elem e, boolean really) {
		boolean ret=true;
		if ( e.getType() == Type.HANGYA) {
			if (really)
				eat(e);
		} else
			ret = false;
		return ret;
	}
	
	public String toString() {
		String out="addAntEater ";
		out+=x+" "+y+" "+getBellySize()+" "+getAntCount()+" ";
		for (Property p:properties) {
			out+=p.name()+" ";
		}
		return out.trim();
	}
	
	public String getShortName() {
		return "S";
	}
	
	public String extraInfo() {
		return "Bend�: "+getBellySize()+"/"+getAntCount();
	}
}
