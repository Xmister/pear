package model;
import java.util.*;

import shared.ErrorLevel;
import shared.PearEventSource;

public abstract class Allat extends Elem {
	protected Dir dir, lastDir; //Dir lehet AUTO. lastDir mindig a legutobbi aktualis lepest tartalmazza.
	/*
	 * Konstruktor
	 */
	public Allat() {
		super();
		dir = Dir.AUTO; 
		lastDir = Dir.Random();
	}
	
	public Allat(int x, int y) { 
		super(x,y); 
		dir = Dir.AUTO;
		lastDir = Dir.Random();
	}
	
	public void addProperty(Property p) {
		if ( !Property.list(Property.NINCSELSZAG,Property.HANGYASZAG).contains(p) )
			super.addProperty(p); //N�h�ny property-t eldoobunk
	}
	
	/*
	 * A mozgast megvalosito metodus
	 */
	protected List<Elem> move() {
		
		List<Elem> ret=null;
		int x=this.x, y=this.y;
		
		if ( lastDir != Dir.NONE) { //Ha mozgunk..
			int i=0;
			Dir original = lastDir;
			do {
				if ( i!= 0 && getDir() == Dir.AUTO ) {
					lastDir = original;
					for (int j=0; j<(i+1)/2; j++)
						lastDir = Dir.getNeighbour(true, lastDir); //Ha AUTO akkor keress�nk m�sik ir�nyt
				}
				int[] res = Dir.getPos(this.x, this.y, lastDir);
				x=res[0];
				y=res[1];
				ret = palya.validPos(this, x, y);
				i++;
			} while (i<7 && ret == null); //6 ir�nyba tudunk menni

			if (ret == null) { //Nem tudunk l�pni 6 pr�b�lkoz�s ut�n sem
				//if ( getDir() != Dir.AUTO )
					PearEventSource.fireError(ErrorLevel.E_WARNING, getType().name()+"("+getX()+","+getY()+"): Tovabblepes nem lehetseges, lehet, hogy beszorultunk?");
					i=0;
			}
			else if ( x != getX() || y != getY() ) {
				for (int j=0; j<ret.size(); j++){
					ret.get(j).visitBy(this,true);
				}
				palya.changeMyPos(this, x, y);
				setX(x);
				setY(y);
			}
			else {
				PearEventSource.fireError(ErrorLevel.E_INFO,getType().name()+"("+getX()+","+getY()+"): Egy helyben maradtunk.");
				ret=null;
			}
		}

		return ret;
	}
	
	/*
	 * Az erzekelest megvalosito metodus
	 */
	protected List<List<Elem>> sense() {
		
		List<List<Elem>> list = palya.getElems(x, y, 3);
		
		return list;
	}
	
	/*
	 * (non-Javadoc)
	 * @see Elem#step()
	 */
	@Override
	public void step() {
		
		move();
		
	}

	public Dir getDir() {
		return dir;
	}

	public void setDir(Dir dir) {
		this.dir = dir;
	}

	public Dir getLastDir() {
		return lastDir;
	}

	public void setLastDir(Dir lastDir) {
		this.lastDir = lastDir;
	}
	
	protected Dir getDir(int x, int y, List<List<Elem>> sensed) {
		Dir vertical, horizontal;
		
		//Horizontalis iranyvalasztas
		if ((this.y - y) % 2 == 0) {
			if ( this.x > x ) horizontal = Dir.B;
			else if ( this.x < x ) horizontal = Dir.J;
			else horizontal = Dir.NONE;
		} else {
			if ( this.x > x-1 ) horizontal = Dir.B;
			else if ( this.x < x-1 ) horizontal = Dir.J;
			else horizontal = Dir.NONE;
		}
		//Vertikalis iranyvalasztas
		if ( this.y > y ) vertical=Dir.F;
		else if ( this.y < y ) vertical=Dir.L;
		else vertical=Dir.NONE;
		Dir nextDir=Dir.get(horizontal, vertical);
		return nextDir;
	}
	
	public String getShortName() {
		return "AL";
	}

}
