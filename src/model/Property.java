package model;

import java.util.Arrays;
import java.util.List;

/*
 * Az elemek/eszkozok lehetseges tulajdonsagai.
 * -------------------
 * Ha egy objektumnak tobb tulajdonsagai is lehet,
 * azok egymas utan lesznek,
 * igy megkonnyitve a kesobbi ellenorzest.
 */
public enum Property {
	MERGEZETT,SZAGTALAN,DEAD,HANGYASZAG,ELELEMSZAG,NINCSELSZAG,LEPETT,FRISS;
	
	private Elem owner=null;
	
	public static List<Property> list(Property... args) {
		return Arrays.asList(args);
	}
	
	Property() {
		
	}
	
	Property(Elem owner) {
		this.owner=owner;
	}

	public Elem getOwner() {
		return owner;
	}
	
}
