package model;
import java.util.Random;


/*
 * Iranyok
 */
public enum Dir {
	//Ervenyes lepesek
	B,J,BF,BL,JF,JL,AUTO,
	//Ervenytelen lepesek, iranyvalasztashoz
	F,L,NONE;
	
	private static Random r = new Random(System.currentTimeMillis());
	
	public static Dir Random() {
		Dir ret = B;
		int rnd = r.nextInt()%31;
		for (int i=0; i < (rnd+1)/2; i++)
			ret = Dir.getNeighbour(rnd%2==0, ret);
		return ret;
	}
	
	public static Dir get(Dir horizontal, Dir vertical) {
		if ( vertical == NONE ) return horizontal;
		if ( horizontal == NONE ) return Dir.valueOf("J"+vertical.name());
		return Dir.valueOf(horizontal.name()+""+vertical.name());
	}
	
	public static int[] getPos(int x, int y, Dir dir) {
		int starty=y;
		switch (dir) { //Hova kell lepni?
			case B:
				x--;
				break;
			case J:
				x++;
				break;
			case BF:
				y--;
				break;
			case JF:
				y--;
				x++;
				break;
			case BL:
				y++;
				break;
			case JL:
				y++;
				x++;
				break;
		}
		if (starty%2!=0 && dir!=Dir.B && dir!=Dir.J) x--;
		return new int[]{x,y};
	}
	
	public static Dir getNeighbour(boolean clockWise, Dir dir) {
		if (clockWise) {
			switch (dir) {
				case J: return JL; //Ennek a sornak a hiánya volt a világon valaha látott legnagyobb bug az emberiség történelmében.
				case B: return BF;
				case BF: return JF;
				case JF: return J;
				case JL: return BL;
				case BL: return B;
				default: return NONE;
			}
		} else {
			switch (dir) {
				case J: return JF; 
				case B: return BL;
				case BL: return JL;
				case JL: return J;
				case JF: return BF;
				case BF: return B;
				default: return NONE;
			}
		}
	}
}
